/**
 * Copyright 2017 Alfa Laboratory Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and limitations under the
 * License.
 */
package com.epam.cucumber.api;

import com.epam.cucumber.ScopedVariables;
import com.epam.cucumber.annotations.Name;
import cucumber.api.Scenario;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Related to CoreScenario class, used to storage pages and variables inside scenario
 */
@Log4j2
public class CoreEnvironment {

    /**
     * Scenario (Cucumber.api), which environment related
     */
    private Scenario scenario;
    /**
     * Variables declared by user inside Scenario ThreadLocal avoid collisions in parallel run
     */
    private ThreadLocal<ScopedVariables> variables = new ThreadLocal<>();
    /**
     * List of pages set by user, available in scenario
     */
    private Pages pages = new Pages();

    /**
     * List of pojos set by user, available in scenario
     */
    private Map<String, Object> pojos = new HashMap<>();

    public CoreEnvironment() {
    }

    public CoreEnvironment(Scenario scenario, boolean uiTest) {
        this.scenario = scenario;
        if (uiTest) {
            initPages();
        } else {
            pojos.putAll(CoreApi.initialize());
        }
    }

    /**
     * find classes annotated "CoraPage.Name", and add links to "pages"
     */
    @SuppressWarnings("unchecked")
    @SneakyThrows
    private void initPages() {
        new AnnotationScanner().getClassesAnnotatedWith(Name.class)
            .stream()
            .map(it -> {
                if (CorePage.class.isAssignableFrom(it)) {
                    return (Class<? extends CorePage>) it;
                } else {
                    throw new IllegalStateException("Class " + it.getName() + " should extends CorePage");
                }
            })
            .forEach(clazz -> pages.put(getClassAnnotationValue(clazz), clazz));
    }

    /**
     * Get value of "CorePage.Name" for class
     *
     * @param c class that should have annotation "CorePage.Name"
     * @return value of the annotation "CorePage.Name"
     */
    private String getClassAnnotationValue(Class<?> c) {
        return Arrays.stream(c.getAnnotationsByType(Name.class))
            .findAny()
            .map(Name::value)
            .orElseThrow(
                () -> new AssertionError(
                    "CorePage.Name annotation cannot be found in class " + c.getClass().getName()));
    }

    /**
     * Add additional info to the report (log.LEVEL.INFO)
     */
    public void write(Object object) {
        scenario.write(String.valueOf(object));
    }

    public ScopedVariables getVars() {
        return getVariables();
    }

    public Object getVar(String name) {
        return getVariables().get(name);
    }

    public void setVar(String name, Object object) {
        getVariables().put(name, object);
    }

    public Scenario getScenario() {
        return scenario;
    }

    public Pages getPages() {
        return pages;
    }

    public CorePage getPage(String name) {
        return pages.get(name);
    }

    public <T extends CorePage> T getPage(Class<T> clazz, String name) {
        return pages.get(clazz, name);
    }

    public String replaceVariables(String textToReplaceIn) {
        return getVariables().replaceVariables(textToReplaceIn);
    }

    private ScopedVariables getVariables() {
        if (variables.get() == null) {
            variables.set(new ScopedVariables());
        }
        return variables.get();
    }
}
