/**
 * Copyright 2017 Alfa Laboratory Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and limitations under the
 * License.
 */
package com.epam.cucumber.api;

import com.codeborne.selenide.Selenide;
import com.google.common.collect.Maps;
import lombok.SneakyThrows;

import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Pages under test storage
 */
public final class Pages {

    /**
     * List of pages under test < Name, Page >
     */
    private Map<String, CorePage> pages;

    /**
     * Current page under test
     */
    private CorePage currentPage;

    public Pages() {
        pages = Maps.newHashMap();
    }

    /**
     * Anonymous methods with page as argument
     *
     * @param clazz                   page class
     * @param checkIfElementsAppeared check all elements without "@Optional" annotation
     */
    public static <T extends CorePage> void withPage(Class<T> clazz, boolean checkIfElementsAppeared,
                                                     Consumer<T> consumer) {
        T page = getPage(clazz, checkIfElementsAppeared);
        consumer.accept(page);
    }

    /**
     * Get page by Class with possibility to check elements
     */
    public static <T extends CorePage> T getPage(Class<T> clazz, boolean checkIfElementsAppeared) {
        T page = Selenide.page(clazz);
        if (checkIfElementsAppeared) {
            page.initialize().isAppeared();
        }
        return page;
    }

    /**
     * Return page under test
     */
    public CorePage getCurrentPage() {
        if (currentPage == null) {
            throw new IllegalStateException("Page is not defined");
        }
        return currentPage.initialize();
    }

    /**
     * Set current page by name
     */
    public void setCurrentPage(CorePage page) {
        this.currentPage = page;
    }

    /**
     * Get page from "pages" by name
     */
    public CorePage get(String pageName) {
        return Selenide.page(getPageFromPagesByName(pageName)).initialize();
    }

    /**
     * Get page by class
     */
    @SuppressWarnings("unchecked")
    public <T extends CorePage> T get(Class<T> clazz, String name) {
        CorePage page = Selenide.page(getPageFromPagesByName(name)).initialize();
        if (!clazz.isInstance(page)) {
            throw new IllegalStateException(name + " page is not a instance of " + clazz + ". Named page is a " + page);
        }
        return (T) page;
    }

    private Map<String, CorePage> getPageMapInstanceInternal() {
        return pages;
    }

    private CorePage getPageFromPagesByName(String pageName) throws IllegalArgumentException {
        CorePage page = getPageMapInstanceInternal().get(pageName);
        if (page == null) {
            throw new IllegalArgumentException(pageName + " page is not declared in a list of available pages");
        }
        return page;
    }

    /**
     * Add page to the "pages" with NULL check
     */
    public <T extends CorePage> void put(String pageName, T page) throws IllegalArgumentException {
        if (page == null) {
            throw new IllegalArgumentException("The page is empty");
        }
        pages.put(pageName, page);
    }

    /**
     * Add page to the "pages" by class
     */
    @SneakyThrows
    public void put(String pageName, Class<? extends CorePage> page) {
        if (page == null) {
            throw new IllegalArgumentException("The page is empty");
        }
        Constructor<? extends CorePage> constructor = page.getDeclaredConstructor();
        constructor.setAccessible(true);
        CorePage p = page.newInstance();
        pages.put(pageName, p);
    }
}
