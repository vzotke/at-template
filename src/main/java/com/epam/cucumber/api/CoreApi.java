package com.epam.cucumber.api;

import com.epam.api.controllers.PojoController;
import com.epam.cucumber.annotations.PojoName;
import com.epam.cucumber.utils.Reflection;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CoreApi {

    /**
     * List of pojos from PojoController, available in scenario
     */
    private static Map<String, Object> namedPojos = new HashMap<>();


    @SuppressWarnings("unchecked")
    public static Map<String, Object> initialize() {

        PojoController tmp = new PojoController();

        new AnnotationScanner().getClassesAnnotatedWith(PojoName.class)
            .stream()
            .map(it -> {
                if (CoreApi.class.isAssignableFrom(it)) {
                    return (Class<? extends CoreApi>) it;
                } else {
                    throw new IllegalStateException("Class " + it.getName() + " should extends CoreApi");
                }
            }).forEach(c -> Arrays.stream(c.getDeclaredFields())
            .forEach(
                f -> namedPojos
                    .put(f.getDeclaredAnnotation(PojoName.class).value(), Reflection.extractFieldValue(f, tmp))));
        return namedPojos;

    }

    private static Object getPojoByName(String name) {
        Object pojo = namedPojos.get(name);
        if (pojo != null) {
            return pojo;
        } else {
            throw new IllegalArgumentException(
                "POJO for name" + name + "cannot be found");
        }
    }

    public static String getJsonForPojoByName(String name) {
        return new Gson().toJson(getPojoByName(name));
    }

}
