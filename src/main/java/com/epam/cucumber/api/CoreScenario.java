/**
 * Copyright 2017 Alfa Laboratory Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and limitations under the
 * License.
 */
package com.epam.cucumber.api;

import static com.epam.setup.AtCoreSetup.DEBUG_MODE;

import com.codeborne.selenide.Selenide;
import com.epam.cucumber.ScopedVariables;
import cucumber.api.Scenario;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Log4j2
public final class CoreScenario {

    private static CoreScenario instance = new CoreScenario();

    /**
     * Test environment, contains: Cucumber.Scenario, variables, declared by user in scenario, which will be tested
     * ThreadLocal is used to avoid thread collision in parallel run
     */
    private static ThreadLocal<CoreEnvironment> environment = new ThreadLocal<>();

    private CoreScenario() {
    }

    public static CoreScenario getInstance() {
        return instance;
    }

    public static void sleep(int seconds) {
        Selenide.sleep(TimeUnit.MILLISECONDS.convert(seconds, TimeUnit.SECONDS));
    }

    /**
     * You can get access to page fields and methods for the specific page that set as argument An example: {@code
     * withPage(CorePage.class, page -> { some actions with CorePage methods});} All page elements visibility
     * verification will be performed
     *
     * @param clazz page class
     */
    public static <T extends CorePage> void withPage(Class<T> clazz, Consumer<T> consumer) {
        withPage(clazz, true, consumer);
    }

    /**
     * You can get access to page fields and methods for the specific page An example: {@code withPage(CorePage.class,
     * page -> { some actions with CorePage methods});} All page elements visibility verification is optional
     *
     * @param clazz                   page class
     * @param checkIfElementsAppeared visibility check flag @Optional
     */
    public static <T extends CorePage> void withPage(Class<T> clazz, boolean checkIfElementsAppeared,
                                                     Consumer<T> consumer) {
        Pages.withPage(clazz, checkIfElementsAppeared, consumer);
    }

    public CoreEnvironment getEnvironment() {
        return environment.get();
    }

    public void setEnvironment(CoreEnvironment coreEnvironment) {
        environment.set(coreEnvironment);
    }

    public void removeEnvironment() {
        environment.remove();
    }

    /**
     * Return the page under test
     */
    public CorePage getCurrentPage() {
        return environment.get().getPages().getCurrentPage();
    }

    /**
     * Set the page to be tested
     */
    public void setCurrentPage(CorePage page) {
        if (page == null) {
            throw new IllegalArgumentException("The page not found " +
                                               "Please heck annotation @Name for target pages");
        }
        environment.get().getPages().setCurrentPage(page);
    }

    /**
     * Return current scenario (Cucumber.api)
     */
    public Scenario getScenario() {
        return getEnvironment().getScenario();
    }

    /**
     * Return list of pages
     */
    public Pages getPages() {
        return this.getEnvironment().getPages();
    }

    public CorePage getPage(String name) {
        return this.getEnvironment().getPage(name);
    }

    /**
     * Add text to the report (log.LEVEL.INFO)
     */
    public void write(Object object) {
        if (DEBUG_MODE) {
            this.getEnvironment().write(object);
        }
    }

    /**
     * Get variables by name which set by user from the pool "variables" in CoreEnvironment
     *
     * @param name - name of target variable
     */
    public Object getVar(String name) {
        Object obj = this.getEnvironment().getVar(name);
        if (obj == null) {
            throw new IllegalArgumentException("The variable " + name + " cannot be found");
        }
        return obj;
    }

    /**
     * Get variable without NULL validation
     */
    public Object tryGetVar(String name) {
        return this.getEnvironment().getVar(name);
    }

    /**
     * Get page by class with possibility to check all elements
     *
     * @param clazz                   - target class to get the page
     * @param checkIfElementsAppeared - visibility check flag
     */
    public <T extends CorePage> T getPage(Class<T> clazz, boolean checkIfElementsAppeared) {
        return Pages.getPage(clazz, checkIfElementsAppeared);
    }

    /**
     * Get page by class without check of visibility of all elements
     *
     * @param clazz - target class to get the page
     */
    public <T extends CorePage> T getPage(Class<T> clazz) {
        return Pages.getPage(clazz, true);
    }

    /**
     * Get page by class and name (both args should be the same)
     *
     * @param clazz - target class to get the page
     * @param name  - page name that set by @Name
     */
    public <T extends CorePage> T getPage(Class<T> clazz, String name) {
        return this.getEnvironment().getPage(clazz, name);
    }

    /**
     * Replace all variables keys from "variables" in the class CoreEnvironment to their values
     *
     * @param stringToReplaceIn String for replace (will not be modified)
     */
    public String replaceVariables(String stringToReplaceIn) {
        return this.getEnvironment().replaceVariables(stringToReplaceIn);
    }

    /**
     * Add variable to the pool "variables" in the class CoreEnvironment
     *
     * @param name   name of variable defined by user, which save the value is key in the pool variables in the class
     *               CoreEnvironment
     * @param object value, that should be saved to variable
     */
    public void setVar(String name, Object object) {
        this.getEnvironment().setVar(name, object);
    }

    /**
     * Get all variables from the pool "variables" in the class CoreEnvironment
     */
    public ScopedVariables getVars() {
        return this.getEnvironment().getVars();
    }
}