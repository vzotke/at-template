/**
 * Copyright 2017 Alfa Laboratory Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and limitations under the
 * License.
 */
package com.epam.cucumber.api;

import static com.epam.core.helpers.PropertyLoader.loadProperty;
import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.ElementsContainer;
import com.codeborne.selenide.SelenideElement;
import com.epam.cucumber.annotations.Name;
import com.epam.cucumber.annotations.Optional;
import com.epam.cucumber.utils.Reflection;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Class for PageObject pattern
 */
@Log4j2
public abstract class CorePage extends ElementsContainer {

    /**
     * Standard time out
     */
    private static final String WAITING_APPEAR_TIMEOUT_IN_MILLISECONDS = "8000";
    /**
     * List of all page elements
     */
    private Map<String, Object> namedElements;
    /**
     * List of elements with "Optional" annotation
     */
    private List<SelenideElement> primaryElements;

    public CorePage() {
        super();
    }

    /**
     * Find element by name in the list of elements
     */
    public static SelenideElement getButtonFromListByName(List<SelenideElement> listButtons, String nameOfButton) {
        List<String> names = new ArrayList<>();
        for (SelenideElement button : listButtons) {
            names.add(button.getText());
        }
        return listButtons.get(names.indexOf(nameOfButton));
    }

    /**
     * Cast element to SelenideElement
     */
    private static SelenideElement castToSelenideElement(Object object) {
        if (object instanceof SelenideElement) {
            return (SelenideElement) object;
        }
        return null;
    }

    private static CorePage castToCorePage(Object object) {
        if (object instanceof CorePage) {
            return (CorePage) object;
        }
        return null;
    }

    /**
     * Return block at the page (annotated "Name")
     */
    public CorePage getBlock(String blockName) {
        return (CorePage) ofNullable(namedElements.get(blockName))
            .orElseThrow(() -> new IllegalArgumentException(
                "Block " + blockName + " is not present on " + this.getClass().getName()));
    }

    /**
     * Return list of blocks at the page (annotated "Name")
     */
    @SuppressWarnings("unchecked")
    public List<CorePage> getBlocksList(String listName) {
        Object value = namedElements.get(listName);
        if (!(value instanceof List)) {
            throw new IllegalArgumentException(
                "List " + listName + " is not present on " + this.getClass().getName());
        }
        Stream<Object> s = ((List) value).stream();
        return s.map(CorePage::castToCorePage).collect(toList());
    }

    /**
     * Return list of block elements (annotated "Name")
     */
    public List<SelenideElement> getBlockElements(String blockName) {
        return getBlock(blockName).namedElements.entrySet().stream()
            .map(x -> ((SelenideElement) x.getValue())).collect(toList());
    }

    /**
     * Get block element by name (annotated "Name")
     */
    public SelenideElement getBlockElement(String blockName, String elementName) {
        return ((SelenideElement) getBlock(blockName).namedElements.get(elementName));
    }

    /**
     * Get element from page (annotated "Name")
     */
    public SelenideElement getElement(String elementName) {
        return (SelenideElement) ofNullable(namedElements.get(elementName))
            .orElseThrow(() -> new IllegalArgumentException(
                "Element " + elementName + " is not declared on " + this.getClass().getName()));
    }

    /**
     * Get list of elements from page by name
     */
    @SuppressWarnings("unchecked")
    public List<SelenideElement> getElementsList(String listName) {
        Object value = namedElements.get(listName);
        if (!(value instanceof List)) {
            throw new IllegalArgumentException(
                "The List " + listName + " is not declared on " + this.getClass().getName());
        }
        Stream<Object> s = ((List) value).stream();
        return s.map(CorePage::castToSelenideElement).collect(toList());
    }

    /**
     * Get text of all elements
     */
    public List<String> getAnyElementsListInnerTexts(String listName) {
        List<SelenideElement> elementsList = getElementsList(listName);
        return elementsList.stream()
            .map(element -> element.getTagName().equals("input")
                            ? element.getValue().trim()
                            : element.innerText().trim()
            )
            .collect(toList());
    }

    /**
     * Get text of element as editable field
     */
    public String getAnyElementText(String elementName) {
        return getAnyElementText(getElement(elementName));
    }

    /**
     * Get text of element as editable field
     */
    public String getAnyElementText(SelenideElement element) {
        if (element.getTagName().equals("input") || element.getTagName().equals("textarea")) {
            return element.getValue();
        } else {
            return element.getText();
        }
    }

    /**
     * Get all input elements from list of elements
     */
    public List<String> getAnyElementsListTexts(String listName) {
        List<SelenideElement> elementsList = getElementsList(listName);
        return elementsList.stream()
            .map(element -> element.getTagName().equals("input")
                            ? element.getValue()
                            : element.getText()
            )
            .collect(toList());
    }

    /**
     * Get all elements without annotation "Optional"
     */
    public List<SelenideElement> getPrimaryElements() {
        if (primaryElements == null) {
            primaryElements = readWithWrappedElements();
        }
        return new ArrayList<>(primaryElements);
    }

    /**
     * Wrapper for CorePage.isAppeared Ex: AkitaPage.appeared().doSomething();
     */
    public final CorePage appeared() {
        isAppeared();
        return this;
    }

    /**
     * Wrapper for CorePage.isDisappeared Ex: CorePage.disappeared().doSomething();
     */
    public final CorePage disappeared() {
        isDisappeared();
        return this;
    }

    /**
     * Check all page elements without annotation "Optional" are displayed from the page
     */
    protected void isAppeared() {
        String timeout = loadProperty("waitingAppearTimeout", WAITING_APPEAR_TIMEOUT_IN_MILLISECONDS);
        getPrimaryElements().parallelStream().forEach(elem ->
                                                          elem.waitUntil(Condition.appear, Integer.parseInt(timeout)));
        eachForm(CorePage::isAppeared);
    }

    private void eachForm(Consumer<CorePage> func) {
        Arrays.stream(getClass().getDeclaredFields())
            .filter(f -> f.getDeclaredAnnotation(Optional.class) == null)
            .forEach(f -> {
                if (CorePage.class.isAssignableFrom(f.getType())) {
                    CorePage
                        corePage =
                        CoreScenario.getInstance().getPage((Class<? extends CorePage>) f.getType()).initialize();
                    func.accept(corePage);
                }
            });
    }

    /**
     * Check all page elements without annotation "Optional" are disappeared from the page
     */
    protected void isDisappeared() {
        String timeout = loadProperty("waitingAppearTimeout", WAITING_APPEAR_TIMEOUT_IN_MILLISECONDS);
        getPrimaryElements().parallelStream().forEach(elem ->
                                                          elem.waitWhile(Condition.exist, Integer.valueOf(timeout)));
    }

    /**
     * Wrapper wor CorePage.isAppearedInIe Ex: CorePage.ieAppeared().doSomething(); Needed for IE
     */
    public final CorePage ieAppeared() {
        isAppearedInIe();
        return this;
    }

    /**
     * Wrapper for CorePage.isDisappearedInIe Ex: CorePage.ieDisappeared().doSomething(); Needed for IE
     */
    public final CorePage ieDisappeared() {
        isDisappearedInIe();
        return this;
    }

    /**
     * Check all page elements without annotation "Optional" are displayed on the page
     */
    protected void isAppearedInIe() {
        String timeout = loadProperty("waitingAppearTimeout", WAITING_APPEAR_TIMEOUT_IN_MILLISECONDS);
        getPrimaryElements().forEach(elem -> elem.waitUntil(Condition.appear, Integer.parseInt(timeout)));
        eachForm(CorePage::isAppearedInIe);
    }

    /**
     * Check all page elements without annotation "Optional" are disappeared from the page
     */
    protected void isDisappearedInIe() {
        String timeout = loadProperty("waitingAppearTimeout", WAITING_APPEAR_TIMEOUT_IN_MILLISECONDS);
        getPrimaryElements().forEach(elem -> elem.waitWhile(Condition.exist, Integer.parseInt(timeout)));
    }

    /**
     * Wrapper for Selenide.waitUntil
     *
     * @param condition Selenide.Condition
     * @param timeout   maximum timeout for element changing condition
     * @param elements  some number of selenide-elements
     */
    public void waitElementsUntil(Condition condition, int timeout, SelenideElement... elements) {
        Spectators.waitElementsUntil(condition, timeout, elements);
    }

    /**
     * Wrapper for Selenide.waitUntil
     *
     * @param elements list selenide-elements
     */
    public void waitElementsUntil(Condition condition, int timeout, List<SelenideElement> elements) {
        Spectators.waitElementsUntil(condition, timeout, elements);
    }


    @Override
    public void setSelf(SelenideElement self) {
        super.setSelf(self);
        initialize();
    }

    public CorePage initialize() {
        namedElements = readNamedElements();
        primaryElements = readWithWrappedElements();
        return this;
    }

    /**
     * Find and initialize page elements
     */
    private Map<String, Object> readNamedElements() {
        checkNamedAnnotations();
        return Arrays.stream(getClass().getDeclaredFields())
            .filter(f -> f.getDeclaredAnnotation(Name.class) != null)
            .peek(this::checkFieldType)
            .collect(toMap(f -> f.getDeclaredAnnotation(Name.class).value(), this::extractFieldValueViaReflection));
    }

    private void checkFieldType(Field f) {
        if (!SelenideElement.class.isAssignableFrom(f.getType())
            && !CorePage.class.isAssignableFrom(f.getType())) {
            checkCollectionFieldType(f);
        }
    }

    private void checkCollectionFieldType(Field f) {
        if (ElementsCollection.class.isAssignableFrom(f.getType())) {
            return;
        } else if (List.class.isAssignableFrom(f.getType())) {
            ParameterizedType listType = (ParameterizedType) f.getGenericType();
            Class<?> listClass = (Class<?>) listType.getActualTypeArguments()[0];
            if (SelenideElement.class.isAssignableFrom(listClass) || CorePage.class.isAssignableFrom(listClass)) {
                return;
            }
        }
        throw new IllegalStateException(
            format("Annotated with @Name field should have type SelenideElement or List<SelenideElement>.\n" +
                   "If it describes block, is should be in the class that extends CorePage.\n" +
                   "Has found filed with the type %s", f.getType()));
    }

    /**
     * Find by annotation "Name"
     */
    private void checkNamedAnnotations() {
        List<String> list = Arrays.stream(getClass().getDeclaredFields())
            .filter(f -> f.getDeclaredAnnotation(Name.class) != null)
            .map(f -> f.getDeclaredAnnotation(Name.class).value())
            .collect(toList());
        if (list.size() != new HashSet<>(list).size()) {
            throw new IllegalStateException(
                "Has been found a few annotation @Name with the same name " + this.getClass().getName());
        }
    }

    /**
     * Find and initialize elements without Optional
     */
    private List<SelenideElement> readWithWrappedElements() {
        return Arrays.stream(getClass().getDeclaredFields())
            .filter(f -> f.getDeclaredAnnotation(Optional.class) == null)
            .map(this::extractFieldValueViaReflection)
            .flatMap(v -> v instanceof List ? ((List<?>) v).stream() : Stream.of(v))
            .map(CorePage::castToSelenideElement)
            .filter(Objects::nonNull)
            .collect(toList());
    }

    private Object extractFieldValueViaReflection(Field field) {
        return Reflection.extractFieldValue(field, this);
    }
}
