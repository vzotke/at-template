package com.epam.cucumber.utils;

/**
 * Thread safe unique id generator
 */
public class UIdGenerator {

    private static long testId;

    /**
     * @return unique long value
     */
    public synchronized static long getUId() {
        long tempId = System.currentTimeMillis();

        if (testId == tempId) {
            try {
                Thread.sleep(1);
                tempId = System.currentTimeMillis();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            getUId();
        }

        testId = tempId;
        return testId;
    }
}
