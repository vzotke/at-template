package com.epam.cucumber.utils;

import com.codeborne.selenide.WebDriverRunner;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Build "environment.properties" file to include environment in Allure report
 */
@Log4j2
public class AllureEnvironmentBuilder {

    /* TODO add system property for allure result folder
         try to use allure.results.directory */
    private static final String path = "target" + File.separator + "allure-results";
    private static boolean isEnvironmentSet = false;


    /**
     * Collect environment info from driver capability such as browser type, version and platform Create
     * "environment.properties" in "target/allure-results" folder
     */
    public static void getAllureEnvironment() {
        if (!isEnvironmentSet) {
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    log.error("\"allure-results\" Folder cannot be created");
                }
            }
            Capabilities cap = ((RemoteWebDriver) WebDriverRunner.getWebDriver()).getCapabilities();
            String browser = cap.getBrowserName().toLowerCase();

            String version = cap.getVersion();
            String os = cap.getPlatform().toString();

            String result = "Browser="
                            + browser
                            + "\n"
                            + "Browser.Version="
                            + version
                            + "\n"
                            + "Stand="
                            + os;

            try (FileWriter file = new FileWriter(path + File.separator + "environment.properties")) {
                file.write(result);
                file.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            isEnvironmentSet = true;
        }
    }

}
