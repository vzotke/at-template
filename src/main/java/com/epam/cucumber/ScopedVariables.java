/**
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * <p style="color: green; font-size: 1.5em">
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p style="color: green; font-size: 1.5em">
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.epam.cucumber;

import static com.epam.core.helpers.PropertyLoader.loadProperty;
import static java.lang.String.format;

import com.epam.cucumber.api.CoreScenario;
import com.google.common.collect.Maps;
import com.google.gson.JsonParser;
import groovy.lang.GroovyShell;

import java.math.BigDecimal;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1 style="color: green; font-size: 2.2em">
 * User variables storage during test scenario run
 * </h1>
 */
public class ScopedVariables {

    public static final String CURVE_BRACES_PATTERN = "\\{([^{}]+)\\}";
    private Map<String, Object> variables = Maps.newHashMap();

    /**
     * @param inputString target String
     * @return new String finds in current String the parameters matching. If found change value to value from
     * properties or variables storage
     */
    public static String resolveVars(String inputString) {
        Pattern p = Pattern.compile(CURVE_BRACES_PATTERN);
        Matcher m = p.matcher(inputString);
        String newString = "";
        while (m.find()) {
            String varName = m.group(1);
            String value = loadProperty(varName, (String) CoreScenario.getInstance().tryGetVar(varName));
            if (value == null) {
                throw new IllegalArgumentException(
                    "Value " + varName +
                    " cannot be found both in application.properties and environment variables");
            }
            newString = m.replaceFirst(value);
            m = p.matcher(newString);
        }
        if (newString.isEmpty()) {
            newString = inputString;
        } else {
            CoreScenario.getInstance().write(format("Value of variable %s = %s", inputString, newString));
        }
        return newString;
    }

    /**
     * @param inputJsonAsString target String
     * @return new String finds in current JSON as String the parameters matching. If found change value to value from
     * properties or variables storage
     */
    public static String resolveJsonVars(String inputJsonAsString) {
        if (isJSONValid(inputJsonAsString)) {
            return inputJsonAsString;
        }
        Pattern p = Pattern.compile(CURVE_BRACES_PATTERN);
        Matcher m = p.matcher(inputJsonAsString);
        String newString = "";
        while (m.find()) {
            String varName = m.group(1);
            String value = loadProperty(varName, (String) CoreScenario.getInstance().tryGetVar(varName));
            if (value == null) {
                CoreScenario.getInstance().write(
                    "Value " + varName +
                    " cannot be found both in application.properties and environment variables");
            }
            newString = m.replaceFirst(value);
            if (isJSONValid(newString)) {
                return newString;
            }
            m = p.matcher(newString);
        }
        if (newString.isEmpty()) {
            newString = inputJsonAsString;
        }
        return newString;
    }

    /**
     * @param jsonInString - String to be validated
     * @return Check if current string is valide JSON
     */
    public static boolean isJSONValid(String jsonInString) {
        try {
            JsonParser parser = new JsonParser();
            parser.parse(jsonInString);
        } catch (com.google.gson.JsonSyntaxException ex) {
            return false;
        }
        return true;
    }

    /**
     * @param expression java/groovy-code, to be executed. Compile and execute in runtime the java/groovy-code. Load in
     *                   memory all created variables, this way can take String with variables from "variables"
     */
    public Object evaluate(String expression) {
        GroovyShell shell = new GroovyShell();
        variables.entrySet().forEach(e -> {
            try {
                shell.setVariable(e.getKey(), new BigDecimal(e.getValue().toString()));
            } catch (NumberFormatException exp) {
                shell.setVariable(e.getKey(), e.getValue());
            }
        });
        return shell.evaluate(expression);
    }

    /**
     * @param textToReplaceIn String to be modified (will not be modified) Replace all keys to values from "variables
     * @return New String after replacement
     */
    public String replaceVariables(String textToReplaceIn) {
        Pattern p = Pattern.compile(CURVE_BRACES_PATTERN);
        Matcher m = p.matcher(textToReplaceIn);
        StringBuffer buffer = new StringBuffer();
        while (m.find()) {
            String varName = m.group(1);
            String value = get(varName).toString();
            m.appendReplacement(buffer, value);
        }
        m.appendTail(buffer);
        return buffer.toString();
    }

    public void put(String name, Object value) {
        variables.put(name, value);
    }

    public Object get(String name) {
        return variables.get(name);
    }

    public void clear() {
        variables.clear();
    }

    public Object remove(String key) {
        return variables.remove(key);
    }

}
