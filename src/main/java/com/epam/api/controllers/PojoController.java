package com.epam.api.controllers;

import com.epam.api.models.Pet;
import com.epam.cucumber.annotations.PojoName;
import com.epam.cucumber.api.CoreApi;

@PojoName("pojo objects")
public class PojoController extends CoreApi {

    @PojoName("new_pet")
    public Pet randomPet = Pet.builder().build();

    @PojoName("random_pet")
    public Pet randomPet2 = Pet.builder().build();

    @PojoName("Pet_to_update")
    public Pet petForUpdate = Pet.builder().id(randomPet.getId()).build();
}
