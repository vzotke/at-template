package com.epam.api.models;

import com.epam.cucumber.utils.UIdGenerator;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Generated;

@Getter
@Setter
@Builder
@ToString
@Generated("com.robohorse.robopojogenerator")
public class TagsItem {

    @Builder.Default
    @SerializedName("id")
    private long id = UIdGenerator.getUId();

    @Builder.Default
    @SerializedName("name")
    private String name = "testTag" + String.valueOf(UIdGenerator.getUId());

}