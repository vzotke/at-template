package com.epam.api.models;

import com.epam.api.controllers.PojoController;
import com.epam.cucumber.api.CoreApi;
import com.epam.cucumber.utils.UIdGenerator;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Generated;

@Getter
@Setter
@Builder
@ToString
@Generated("com.robohorse.robopojogenerator")
public class Pet {

    @Builder.Default
    @SerializedName("id")
    private long id = UIdGenerator.getUId();

    @Builder.Default
    @SerializedName("category")
    private Category category = Category.builder().build();

    @Builder.Default
    @SerializedName("name")
    private String name = "testPet" + String.valueOf(UIdGenerator.getUId());

    @Builder.Default
    @SerializedName("photoUrls")
    private List<String> photoUrls = Arrays.asList("String 1", "String 2");

    @Builder.Default
    @SerializedName("tags")
    private List<TagsItem> tags = new ArrayList<>(Arrays.asList(TagsItem.builder().build()));

    @Builder.Default
    @SerializedName("status")
    private String status = "available";
}