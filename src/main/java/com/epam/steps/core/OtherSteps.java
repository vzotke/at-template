package com.epam.steps.core;

import static com.codeborne.selenide.Selenide.sleep;
import static com.epam.core.helpers.PropertyLoader.getPropertyOrValue;
import static com.epam.core.helpers.PropertyLoader.loadProperty;
import static com.epam.core.helpers.PropertyLoader.tryLoadProperty;
import static java.util.Objects.isNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;

import com.epam.cucumber.api.CoreScenario;
import com.google.gson.Gson;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1 style="color: green; font-size: 2.2em">
 * Utils steps </>
 */
public class OtherSteps {

    private static CoreScenario coreScenario = CoreScenario.getInstance();

    /**
     * <p style="color: green; font-size: 1.5em">
     *
     * @return Get "Downloads" folder
     * </p>
     */
    public static File getDownloadsDir() {
        String homeDir = System.getProperty("user.home");
        return new File(homeDir + "/Downloads");
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     *
     * @param filesToDelete files array to be deleted</p>
     */
    public static void deleteFiles(File[] filesToDelete) {
        for (File file : filesToDelete) {
            if (!file.delete()) {
                try {
                    throw new IOException("File cannot be deleted");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ;
        }
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     *
     * @param maxValueInRange maximum value of range to be returned value from 0 to maxValueInRange</p>
     */
    public static int getRandom(int maxValueInRange) {
        return (int) (Math.random() * maxValueInRange);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Returns String with random set of chars with target length and localisation Takes 'ru' and 'en' for others en
     * will be used
     *
     * @param length
     * @param lang   </p>
     */
    public static String getRandCharSequence(int length, String lang) {

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char symbol = charGenerator(lang);
            builder.append(symbol);
        }
        return builder.toString();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Return random char of target alphabet
     *
     * @param lang </p>
     */
    public static char charGenerator(String lang) {
        Random random = new Random();
        if (lang.equals("ru")) {
            return (char) (1072 + random.nextInt(32));
        } else {
            return (char) (97 + random.nextInt(26));
        }
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check string matches pattern
     *
     * @param pattern
     * @param str     </p>
     */
    public static boolean isTextMatches(String str, String pattern) {
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Returns locator to be found with case ignored text
     *
     * @param expectedText </p>
     */
    public static String getTranslateNormalizeSpaceText(String expectedText) {
        StringBuilder text = new StringBuilder();
        text.append("//*[contains(translate(normalize-space(text()), ");
        text.append("'ABCDEFGHIJKLMNOPQRSTUVWXYZАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ', ");
        text.append("'abcdefghijklmnopqrstuvwxyzабвгдеёжзийклмнопрстуфхчшщъыьэюя'), '");
        text.append(expectedText.toLowerCase());
        text.append("')]");
        return text.toString();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     *
     * @return Returns value from property file if not found from coreScenario variables, if nothing found returns
     * argument</p>
     */
    public static String getPropertyOrStringVariableOrValue(String propertyNameOrVariableNameOrValue) {
        String propertyValue = tryLoadProperty(propertyNameOrVariableNameOrValue);
        String variableValue = (String) CoreScenario.getInstance().tryGetVar(propertyNameOrVariableNameOrValue);

        boolean
            propertyCheck =
            checkResult(propertyValue, "Variable " + propertyNameOrVariableNameOrValue + " from property file");
        boolean variableCheck = checkResult(variableValue, "Scenario's variable " + propertyNameOrVariableNameOrValue);

        return propertyCheck ? propertyValue : (variableCheck ? variableValue : propertyNameOrVariableNameOrValue);
    }

    public static boolean checkResult(String result, String message) {
        if (isNull(result)) {
            coreScenario.write(message + " is not found");
            return false;
        }
        coreScenario.write(message + " = " + result);
        CoreScenario.getInstance().write(message + " = " + result);
        return true;
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set current date in scenario variables.
     *
     * @param variableName name of variable
     * @param dateFormat   date format
     *                     </p>
     */
    @And("^the variable \"([^\"]*)\" is set with current date in format: \"([^\"]*)\"$")
    public void setCurrentDate(String variableName, String dateFormat) {
        long date = System.currentTimeMillis();
        setDate(date, variableName, dateFormat);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set current date with negative shift to scenario variables.
     *
     * @param variableName name of variable
     * @param hour         negative shift
     * @param dateFormat   date format</p>
     */
    @And("^the current date has been set to variable \"([^\"]*)\" with negative shift (\\d+) (?:hour|hours) in format \"([^\"]*)\"$")
    public void setMinusDate(String variableName, int hour, String dateFormat) {
        long time = new Date(System.currentTimeMillis() - hour * 1000 * 3600).getTime();
        setDate(time, variableName, dateFormat);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set current date with positive shift to scenario variables
     *
     * @param variableName name of variable
     * @param hour         positive shift
     * @param dateFormat   date format</p>
     */
    @And("^the current date has been set to variable  \"([^\"]*)\" with positive shift  (\\d+) (?:hour|hours) in format \"([^\"]*)\"$")
    public void setPlusDate(String variableName, int hour, String dateFormat) {
        long time = new Date(System.currentTimeMillis() + hour * 1000 * 3600).getTime();
        setDate(time, variableName, dateFormat);
    }

    private void setDate(long date, String variableName, String dateFormat) {
        String currentStringDate;
        try {
            currentStringDate = new SimpleDateFormat(dateFormat).format(date);
        } catch (IllegalArgumentException ex) {
            currentStringDate = new SimpleDateFormat("dd.MM.yyyy").format(date);
            coreScenario.write("Wrong date format. Default format will be used: dd.MM.yyyy");
        }

        coreScenario.setVar(variableName, currentStringDate);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set value to one of scenario variable. Use case: Set login of user
     *
     * @param variableName Name of variable
     * @param value        Value\</p>
     */
    @And("^the value of the variable \"([^\"]*)\" has been changed to \"(.*)\"$")
    public void setVariable(String variableName, String value) {
        value = getPropertyOrValue(value);
        coreScenario.setVar(variableName, value);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check 2 variables are equal
     *
     * @param firstVariableName  the first variable
     * @param secondVariableName the second variable</p>
     */
    @Then("^the values of the variables \"([^\"]*)\" и \"([^\"]*)\" equal")
    public void compareTwoVariables(String firstVariableName, String secondVariableName) {
        String firstValueToCompare = coreScenario.getVar(firstVariableName).toString();
        String secondValueToCompare = coreScenario.getVar(secondVariableName).toString();
        assertThat(
            String.format("values of variables [%s] and [%s] don't match", firstVariableName, secondVariableName),
            firstValueToCompare, equalTo(secondValueToCompare));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * heck 2 variables are not equal
     *
     * @param firstVariableName  the first variable
     * @param secondVariableName the second variable</p>
     */
    @Then("^the values of the variables \"([^\"]*)\" и \"([^\"]*)\" are not equal$")
    public void checkingTwoVariablesAreNotEquals(String firstVariableName, String secondVariableName) {
        String firstValueToCompare = coreScenario.getVar(firstVariableName).toString();
        String secondValueToCompare = coreScenario.getVar(secondVariableName).toString();
        assertThat(String.format("values of variables [%s] and [%s] are equal", firstVariableName, secondVariableName),
                   firstValueToCompare, Matchers.not(equalTo(secondValueToCompare)));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check if true
     *
     * @param expression expression from property, or scenario variable or step vsalue e.g. string1.equals(string2) OR
     *                   string.equals("string") any Java-expression, that returns boolean</p>
     */
    @Then("^is true, that \"([^\"]*)\"$")
    public void expressionExpression(String expression) {
        coreScenario.getVars().evaluate("assert(" + expression + ")");
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * The value from property file has been save to variable
     *
     * @param propertyVariableName key in the application.properties
     * @param variableName         name of the target variable, value from application.properties will be saved to the
     *                             coreScenario variable to feature use</p>
     */
    @And("^the value \"([^\"]*)\" has been saved from property file to variable \"([^\"]*)\"$")
    public void saveValueToVar(String propertyVariableName, String variableName) {
        propertyVariableName = loadProperty(propertyVariableName);
        coreScenario.setVar(variableName, propertyVariableName);
        coreScenario.write("Value of the variables" + propertyVariableName);
    }

    /**
     * Check if variable equals to property value
     */
    @Then("^the values form variable \"([^\"]*)\" and property file \"([^\"]*)\" are equal$")
    public void checkIfValueFromVariableEqualPropertyVariable(String envVarible, String propertyVariable) {
        assertThat("Variables " + envVarible + " and " + propertyVariable + " are not equal",
                   (String) coreScenario.getVar(envVarible), equalToIgnoringCase(loadProperty(propertyVariable)));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Wait
     *
     * @param seconds seconds
     *                </p>
     */
    @When("^wait for (\\d+) (?:second|seconds)")
    public void waitForSeconds(long seconds) {
        sleep(1000 * seconds);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * In progress</p>
     */
    @And("^test development is in progress$")
    public void pendingException() {
        throw new cucumber.api.PendingException("test development is in progress");
    }

    public String getJsonFromPojo(Object pojo){
        String json = new Gson().toJson(pojo);
        CoreScenario.getInstance().write("Object" + pojo);
        CoreScenario.getInstance().write("JSON has been built: " + json);
        return json;
    }
}
