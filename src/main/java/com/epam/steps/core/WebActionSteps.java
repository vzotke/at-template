package com.epam.steps.core;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.atBottom;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.Selenide.switchTo;
import static com.codeborne.selenide.WebDriverRunner.isIE;
import static com.codeborne.selenide.WebDriverRunner.url;
import static com.epam.core.helpers.PropertyLoader.loadProperty;
import static com.epam.core.helpers.PropertyLoader.loadValueFromFileOrPropertyOrVariableOrDefault;
import static com.epam.cucumber.ScopedVariables.resolveVars;
import static com.epam.steps.core.OtherSteps.getPropertyOrStringVariableOrValue;
import static com.epam.steps.core.OtherSteps.getRandCharSequence;
import static com.epam.steps.core.OtherSteps.getTranslateNormalizeSpaceText;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.epam.cucumber.api.CoreScenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <h1 style="color: green; font-size: 2.2em">
 * WEB steps
 * </h1>
 *
 * <p style="color: green; font-size: 1.5em">
 * There is variable storage in coreScenario. To save/get variables setVar/getVar methods can be used Each page should
 * be added to according class which extends CorePage. Each page/element should have english name in @Name annotation to
 * be found by human readable name not by selector Selectors should be placed only in page class, NOT in steps In steps
 * interaction is possible only via name from @Name annotation</p>
 */
public class WebActionSteps {

    private CoreScenario coreScenario = CoreScenario.getInstance();

    /**
     * <p style="color: green; font-size: 1.5em">
     * Go to page via link. Step has assert that target page is loaded. Link can be set as String or as a Key from
     * application.properties
     * </p>
     */
    @And("^open the page \"([^\"]*)\" by the link \"([^\"]*)\"$")
    public void goToSelectedPageByLink(String pageName, String urlOrName) {
        String address = loadProperty(urlOrName, resolveVars(urlOrName));
        coreScenario.write(" url = " + address);
        open(address);
        loadPage(pageName);
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Go to page via link in the new tab. Step has assert that target page is loaded. Link can be set as String or as a
     * Key from application.properties
     * </p>
     */
    @And("^open the page \"([^\"]*)\" in the new tab by the link \"([^\"]*)\"$")
    public void goToSelectedPageByLinkNewTab(String pageName, String urlOrName) {
        String url = resolveVars(getPropertyOrStringVariableOrValue(urlOrName));
        coreScenario.write(" url = " + url);
        ((JavascriptExecutor) WebDriverRunner.getWebDriver())
            .executeScript("window.open('" + url + "','_blank');");
        loadPage(pageName);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Go to page via click link/button and verify that page is loaded
     * </p>
     */
    @And("^open the page \"([^\"]*)\" by click to the (?:link|button) \"([^\"]*)\"$")
    public void urlClickAndCheckRedirection(String pageName, String elementName) {
        coreScenario.getCurrentPage().getElement(elementName).click();
        loadPage(pageName);
        coreScenario.write(" url = " + url());
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Verify that all elements that described at page class with annotation @Name but without @Optional appear on the
     * page in WAITING_APPEAR_TIMEOUT, that is equal to  "waitingAppearTimeout" from application.properties. If not
     * found, default value is 8 seconds
     * </p>
     *
     * @param nameOfPage name of page|block|form|tab
     */
    @Then("^the (?:page|block|form|tab) \"([^\"]*)\" is loaded$")
    public void loadPage(String nameOfPage) {
        coreScenario.setCurrentPage(coreScenario.getPage(nameOfPage));
        if (isIE()) {
            coreScenario.getCurrentPage().ieAppeared();
        } else {
            coreScenario.getCurrentPage().appeared();
        }
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * click on target element
     *
     * @param elementName name button|block|link|checkbox</p>
     */
    @And("^click on (?:button|block|link|checkbox) \"([^\"]*)\"$")
    public void clickOnElement(String elementName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        element.click();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * click on target element if not Displayed try to click to parent element
     *
     * @param elementName name button|block|link|checkbox</p>
     */
    @And("^smart click on (?:button|block|link|checkbox) \"([^\"]*)\"$")
    public void clickElementOrParent(String elementName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        if (element.isDisplayed()) {
            element.click();
        } else {
            element.parent().click();
        }
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Send keyboard button
     * </p>
     */
    @And("^send keyboard button \"([^\"]*)\"$")
    public void pushButtonOnKeyboard(String buttonName) {
        Keys key = Keys.valueOf(buttonName.toUpperCase());
        switchTo().activeElement().sendKeys(key);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * <p>
     * Emulate keys combination. To send Ctrl+A, the table should contain | CONTROL | | a       |
     *
     * @param keyNames key names</p>
     */
    @And("^send keys$")
    public void pressKeyCombination(List<String> keyNames) {
        Iterable<CharSequence> listKeys = keyNames.stream()
            .map(this::getKeyOrCharacter)
            .collect(Collectors.toList());
        String combination = Keys.chord(listKeys);
        switchTo().activeElement().sendKeys(combination);
    }

    private CharSequence getKeyOrCharacter(String key) {
        try {
            return Keys.valueOf(key.toUpperCase());
        } catch (IllegalArgumentException ex) {
            return key;
        }
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set value to the target field. Field will be cleaned before using
     * </p>
     */
    @When("^in the field \"([^\"]*)\" send value \"(.*)\"$")
    public void setFieldValue(String elementName, String value) {
        value = getPropertyOrStringVariableOrValue(value);
        SelenideElement valueInput = coreScenario.getCurrentPage().getElement(elementName);
        cleanField(elementName);
        valueInput.setValue(value);
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Send keys to the target field. Field will be cleaned before using
     * </p>
     */
    @When("^in the field \"([^\"]*)\" send keys \"(.*)\"$")
    public void sendKeys(String elementName, String value) {
        value = getPropertyOrStringVariableOrValue(value);
        SelenideElement valueInput = coreScenario.getCurrentPage().getElement(elementName);
        cleanField(elementName);
        valueInput.sendKeys(value);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Набирается значение посимвольно (в приоритете: из property, из переменной сценария, значение аргумента) в
     * заданное поле. Перед использованием поле нужно очистить
     * </p>
     */
    @Then("^in the field \"([^\"]*)\" send value char by char \"([^\"]*)\"$")
    public void sendKeysCharacterByCharacter(String elementName, String value) {
        value = getPropertyOrStringVariableOrValue(value);
        SelenideElement valueInput = coreScenario.getCurrentPage().getElement(elementName);
        valueInput.clear();

        WebDriver webDriver = WebDriverRunner.getWebDriver();

        sleep(200);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", valueInput);
        sleep(200);
        valueInput.sendKeys(Keys.CONTROL, "a");
        sleep(200);
        valueInput.sendKeys(Keys.DELETE);
        sleep(200);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", valueInput);
        sleep(200);
        valueInput.clear();

        valueInput.sendKeys(Keys.CONTROL, "a");
        for (char character : value.toCharArray()) {
            valueInput.sendKeys(String.valueOf(character));
            sleep(100);
        }
        sleep(200);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * clear target field
     * </p>
     */
    @When("^clean the field \"([^\"]*)\"$")
    public void cleanField(String nameOfField) {
        SelenideElement valueInput = coreScenario.getCurrentPage().getElement(nameOfField);
        valueInput.clear();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Hover on element
     * </p>
     */
    @When("^hover on (?:field|element) \"([^\"]*)\"$")
    public void elementHover(String elementName) {
        SelenideElement field = coreScenario.getCurrentPage().getElement(elementName);
        field.hover();
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * add string to the field
     * </p>
     */
    @When("^to the field \"([^\"]*)\" add value \"(.*)\"$")
    public void addValue(String elementName, String value) {
        value = getPropertyOrStringVariableOrValue(value);
        SelenideElement field = coreScenario.getCurrentPage().getElement(elementName);
        String oldValue = field.getValue();
        if (oldValue.isEmpty()) {
            oldValue = field.getText();
        }
        field.setValue("");
        field.setValue(oldValue + value);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Click element by text
     * </p>
     */
    @And("^Click on element with text \"(.*)\"$")
    public void findElement(String text) {
        $(By.xpath(getTranslateNormalizeSpaceText(getPropertyOrStringVariableOrValue(text)))).click();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set current date into field in format, if format is wrong will be used default dd.MM.yyyy
     * </p>
     */
    @When("^fill the element \"([^\"]*)\" with the date in format \"([^\"]*)\"$")
    public void currentDate(String fieldName, String dateFormat) {
        long date = System.currentTimeMillis();
        String currentStringDate;
        try {
            currentStringDate = new SimpleDateFormat(dateFormat).format(date);
        } catch (IllegalArgumentException ex) {
            currentStringDate = new SimpleDateFormat("dd.MM.yyyy").format(date);
            coreScenario.write("Wrong date format. Default format will be used: dd.MM.yyyy");
        }
        SelenideElement valueInput = coreScenario.getCurrentPage().getElement(fieldName);
        valueInput.setValue("");
        valueInput.setValue(currentStringDate);
        coreScenario.write("Current date is " + currentStringDate);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set value from clipboard with SHIFT + INSERT combination
     * </p>
     */
    @When("^set the value from clipboard \"([^\"]*)\" into element \"([^\"]*)\" with hot keys$")
    public void pasteValueToTextField(String value, String fieldName) {
        value = getPropertyOrStringVariableOrValue(value);
        ClipboardOwner clipboardOwner = (clipboard, contents) -> {
        };
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection stringSelection = new StringSelection(value);
        clipboard.setContents(stringSelection, clipboardOwner);
        coreScenario.getCurrentPage().getElement(fieldName).sendKeys(Keys.chord(Keys.SHIFT, Keys.INSERT));
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Try to find file in /Downloads by text in the name. Can take RegEX</p>
     */
    @Then("^the file by path \"(.*)\" loaded in the field \"(.*)\"$")
    public void uploadFile(String path, String fieldName) {
        coreScenario.getCurrentPage()
            .getElement(fieldName)
            .uploadFile(new File(path));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Scroll to element
     * </p>
     */
    @Then("^scroll the page to the element \"([^\"]*)\"")
    public void scrollPageToElement(String elementName) {
        coreScenario.getCurrentPage().getElement(elementName).scrollTo();
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Set to the field random number of latin or cyrillic chars</p>
     */
    @When("^to the field \"([^\"]*)\" is sent (\\d+) random chars (?:cyrillic|latin)$")
    public void setRandomCharSequence(String elementName, int seqLength, String lang) {
        SelenideElement valueInput = coreScenario.getCurrentPage().getElement(elementName);
        cleanField(elementName);

        if (lang.equals("cyrillic")) {
            lang = "ru";
        } else {
            lang = "en";
        }
        String charSeq = getRandCharSequence(seqLength, lang);
        valueInput.setValue(charSeq);
        coreScenario.write("String of random chars is: " + charSeq);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set to the field random number of latin or cyrillic chars and save it to the variable</p>
     */
    @When("^to the field \"([^\"]*)\" is sent (\\d+) random chars (?:cyrillic|latin) and hsa been saved to the variable \"([^\"]*)\"$")
    public void setRandomCharSequenceAndSaveToVar(String elementName, int seqLength, String lang, String varName) {
        SelenideElement valueInput = coreScenario.getCurrentPage().getElement(elementName);
        cleanField(elementName);

        if (lang.equals("cyrillic")) {
            lang = "ru";
        } else {
            lang = "en";
        }
        String charSeq = getRandCharSequence(seqLength, lang);
        valueInput.setValue(charSeq);
        coreScenario.setVar(varName, charSeq);
        coreScenario.write("String of random chars is:" + charSeq);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set in field random integer set with fixed length
     * </p>
     */
    @When("^to the field \"([^\"]*)\" is sent (\\d+) random integers $")
    public void inputRandomNumSequence(String elementName, int seqLength) {
        SelenideElement valueInput = coreScenario.getCurrentPage().getElement(elementName);
        cleanField(elementName);
        String numSeq = RandomStringUtils.randomNumeric(seqLength);
        valueInput.setValue(numSeq);
        coreScenario.write(String.format("In field [%s] is sent the value: [%s]", elementName, numSeq));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Set in field random integer set with fixed length and save it to variable
     * </p>
     */
    @When("^to the field \"([^\"]*)\" is sent (\\d+) random integers and has been saved the value to variable: \"([^\"]*)\"$")
    public void inputAndSetRandomNumSequence(String elementName, int seqLength, String varName) {
        SelenideElement valueInput = coreScenario.getCurrentPage().getElement(elementName);
        cleanField(elementName);
        String numSeq = RandomStringUtils.randomNumeric(seqLength);
        valueInput.setValue(numSeq);
        coreScenario.setVar(varName, numSeq);
        coreScenario.write(String.format("In field [%s] is sent the value: [%s]\" and has been saved to variable [%s]",
                                         elementName, numSeq, varName));
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Run js-script with description of the loginc in js.executeScript Can be sent as argument or or value from
     * application.properties
     * </p>
     */
    @When("^run the js-script \"([^\"]*)\"")
    public void executeJsScript(String scriptName) {
        String content = loadValueFromFileOrPropertyOrVariableOrDefault(scriptName);
        Selenide.executeJavaScript(content);
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Scroll down the page each second as long as element is displayed. If footer displayed and element not found trows
     * an exception.
     * </p>
     */
    @And("^scroll the page down to display the element \"([^\"]*)\"$")
    public void scrollWhileElemNotFoundOnPage(String elementName) {
        SelenideElement el = null;
        do {
            el = coreScenario.getCurrentPage().getElement(elementName);
            if (el.exists()) {
                break;
            }
            executeJavaScript("return window.scrollBy(0, 250);");
            sleep(1000);
        } while (!atBottom());
        el.shouldHave(enabled);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Scroll down the page each second as long as element is displayed. Try to find element by text in scenario
     * variables or properties if not found use argument If footer displayed and element not found trows an exception.
     * </p>
     */
    @And("^scroll the page down to display the element with the text \"([^\"]*)\"$")
    public void scrollWhileElemWithTextNotFoundOnPage(String expectedValue) {
        SelenideElement el = null;
        do {
            el = $(By.xpath(getTranslateNormalizeSpaceText(getPropertyOrStringVariableOrValue(expectedValue))));
            if (el.exists()) {
                break;
            }
            executeJavaScript("return window.scrollBy(0, 250);");
            sleep(1000);
        } while (!atBottom());
        el.shouldHave(enabled);
    }


    /**
     * Click button and upload selected file. Selector MUST be related to input of element. Path to file can be used.
     * E.g. src/test/resources/example.pdf
     * </p>
     */
    @When("^click on button \"([^\"]*)\" and upload the file \"([^\"]*)\"$")
    public void clickOnButtonAndUploadFile(String buttonName, String fileName) {
        String file = loadValueFromFileOrPropertyOrVariableOrDefault(fileName);
        File attachmentFile = new File(file);
        coreScenario.getCurrentPage().getElement(buttonName).uploadFile(attachmentFile);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Click target element in the block
     *
     * @param elementName name of element
     * @param blockName   name of block</p>
     */
    @And("^click on (?:button|field) \"([^\"]*)\" in the block \"([^\"]*)\"$")
    public void clickOnElementInBlock(String elementName, String blockName) {
        coreScenario.getCurrentPage().getBlock(blockName).getElement(elementName).click();
    }
}
