package com.epam.steps.core;

import static com.codeborne.selenide.Condition.not;
import static com.epam.steps.core.OtherSteps.getPropertyOrStringVariableOrValue;
import static com.epam.steps.core.OtherSteps.getRandom;
import static com.epam.steps.core.WebTestConfig.DEFAULT_TIMEOUT;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.epam.cucumber.api.CoreScenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <h1 style="color: green; font-size: 2.2em">
 * WEB list interaction steps
 * </h1>
 *
 * <p style="color: green; font-size: 1.5em">
 * There is variable storage in coreScenario. To save/get variables setVar/getVar methods can be used Each page should
 * be added to according class which extends CorePage. Each page/element should have english name in @Name annotation to
 * be found by human readable name not by selector Selectors should be placed only in page class, NOT in steps In steps
 * interaction is possible only via name from @Name annotation</p>
 */
public class WebListSteps {

    private CoreScenario coreScenario = CoreScenario.getInstance();


    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that values from field is contained in the list of elements from variable storage by the key
     * </p>
     *
     * @param variableListName name of variable
     * @param elementName      name of field|element
     */
    @SuppressWarnings("unchecked")
    @Then("^the list from variable \"([^\"]*)\" contains value (?:field|element) \"([^\"]*)\"$")
    public void checkIfListContainsValueFromField(String variableListName, String elementName) {
        String actualValue = coreScenario.getCurrentPage().getAnyElementText(elementName);
        List<String> listFromVariable = ((List<String>) coreScenario.getVar(variableListName));
        assertTrue(
            String.format("the list from variable [%s] doesn't contain fields [%s]", variableListName, elementName),
            listFromVariable.contains(actualValue));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that list appears in DEFAULT_TIMEOUT. In case if "waitingCustomElementsTimeout" in application.properties
     * is not set, default timeout will be set to 10 seconds
     *
     * @param elementName name of element
     *                    </p>
     */
    @Then("^the list \"([^\"]*)\" is displayed on the page$")
    public void listIsPresentedOnPage(String elementName) {
        coreScenario.getCurrentPage().waitElementsUntil(
            Condition.appear, DEFAULT_TIMEOUT, coreScenario.getCurrentPage().getElementsList(elementName)
        );
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that list contains all texts from table. To get text is used getText()
     * </p>
     */
    @Then("^the list \"([^\"]*)\" consists of table elements by text$")
    public void checkIfListConsistsOfTableElements(String listName, List<String> textTable) {
        List<String> actualValues = coreScenario.getCurrentPage().getAnyElementsListTexts(listName);
        int numberOfTypes = actualValues.size();
        assertThat(String.format("Unexpected number of elements in the list [%s]", listName), textTable,
                   hasSize(numberOfTypes));
        assertTrue(
            String.format("The values in the list [%s] don't equal to expected values from table", listName),
            actualValues.containsAll(textTable));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that list contains all texts from table. To get text is used innerText()
     * </p>
     */
    @Then("^the list \"([^\"]*)\" consists of table elements by full text$")
    public void checkIfListInnerTextConsistsOfTableElements(String listName, List<String> textTable) {
        List<String> actualValues = coreScenario.getCurrentPage().getAnyElementsListInnerTexts(listName);
        int numberOfTypes = actualValues.size();
        assertThat(String.format("Unexpected number of elements in the list [%s]", listName), textTable,
                   hasSize(numberOfTypes));
        assertTrue(String
                       .format("The values in the list %s: %s don't equal to expected values from table %s",
                               listName, actualValues, textTable),
                   actualValues.containsAll(textTable));
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Select list item by value
     * </p>
     */
    @Then("^in the list \"([^\"]*)\" element with the (?:text|value) \"(.*)\" has been selected$")
    public void checkIfSelectedListElementMatchesValue(String listName, String expectedValue) {
        final String value = getPropertyOrStringVariableOrValue(expectedValue);
        List<SelenideElement> listOfElementsFromPage = coreScenario.getCurrentPage().getElementsList(listName);
        List<String> elementsText = listOfElementsFromPage.stream()
            .map(element -> element.getText().trim())
            .collect(toList());
        listOfElementsFromPage.stream()
            .filter(element -> element.getText().trim().equalsIgnoreCase(value))
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException(
                String.format("The element [%s] is not found in the list %s: [%s] ", value, listName, elementsText)))
            .click();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Select list item by text, ignore case
     * </p>
     */
    @Then("^in the list \"([^\"]*)\" element with the text \"([^\"]*)\" has been selected$")
    public void selectElementInListIfFoundByText(String listName, String expectedValue) {
        final String value = getPropertyOrStringVariableOrValue(expectedValue);
        List<SelenideElement> listOfElementsFromPage = coreScenario.getCurrentPage().getElementsList(listName);
        List<String> elementsListText = listOfElementsFromPage.stream()
            .map(element -> element.getText().trim().toLowerCase())
            .collect(toList());
        listOfElementsFromPage.stream()
            .filter(element -> element.getText().trim().toLowerCase().contains(value.toLowerCase()))
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException(
                String.format("The element [%s] cannot be found in the list %s: [%s] ", value, listName,
                              elementsListText)))
            .click();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check list elements contains elements from expected list, ignore order. get text with innerText()
     * </p>
     */
    @SuppressWarnings("unchecked")
    @Then("^the list \"([^\"]*)\" equals to list: \"([^\"]*)\"$")
    public void checkListInnerTextCorrespondsToListFromVariable(String listName, String listVariable) {
        List<String> expectedList = new ArrayList<>((List<String>) coreScenario.getVar(listVariable));
        List<String> actualList = new ArrayList<>(coreScenario.getCurrentPage().getAnyElementsListInnerTexts(listName));
        assertThat(String.format("List size %s = %s, but expected value = %s", listName,
                                 actualList.size(), expectedList.size()), actualList,
                   hasSize(expectedList.size()));
        assertThat(String.format("List from page %s: %s is not equal to list from variable %s:%s",
                                 listName, actualList, listVariable, expectedList)
            , actualList, containsInAnyOrder(expectedList.toArray()));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that element is not displayed on the page
     * </p>
     */
    @Then("^the (?:field|dropdown list|element) \"([^\"]*)\" is not displayed on the page$")
    public void elementIsNotVisible(String elementName) {
        coreScenario.getCurrentPage().waitElementsUntil(
            not(Condition.appear), DEFAULT_TIMEOUT, coreScenario.getCurrentPage().getElement(elementName)
        );
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that list from page equals to list from variable. ignore order.
     * </p>
     */
    @SuppressWarnings("unchecked")
    @Then("^the list \"([^\"]*)\" from page equals to list form variable \"([^\"]*)\"$")
    public void compareListFromUIAndFromVariable(String listName, String listVariable) {
        HashSet<String> expectedList = new HashSet<>((List<String>) coreScenario.getVar(listVariable));
        HashSet<String> actualList = new HashSet<>(coreScenario.getCurrentPage().getAnyElementsListTexts(listName));
        assertThat(String
                       .format("The list from the page [%s] doesn't equal to list from variable [%s]", listName,
                               listVariable), actualList, equalTo(expectedList));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Select random list item
     * </p>
     */
    @Then("^random element of list \"([^\"]*)\" has been selected$")
    public void selectRandomElementFromList(String listName) {
        List<SelenideElement> listOfElementsFromPage = coreScenario.getCurrentPage().getElementsList(listName);
        listOfElementsFromPage.get(getRandom(listOfElementsFromPage.size()))
            .shouldBe(Condition.visible).click();
        coreScenario.write("Random element has been selected: " + listOfElementsFromPage);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Select list element by number, started from 1
     * </p>
     */
    @Then("^the (\\d+)-th element of the list \"([^\"]*)\" has been selected$")
    public void selectElementNumberFromList(Integer elementNumber, String listName) {
        List<SelenideElement> listOfElementsFromPage = coreScenario.getCurrentPage().getElementsList(listName);
        SelenideElement elementToSelect;
        int selectedElementNumber = elementNumber - 1;
        if (selectedElementNumber < 0 || selectedElementNumber >= listOfElementsFromPage.size()) {
            throw new IndexOutOfBoundsException(
                String.format("The list %s doesn't have element with number %s. Actual number of elements = %s",
                              listName, elementNumber, listOfElementsFromPage.size()));
        }
        elementToSelect = listOfElementsFromPage.get(selectedElementNumber);
        elementToSelect.shouldBe(Condition.visible).click();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Compare size of list to expected
     * </p>
     */
    @Then("^the list \"([^\"]*)\" contains (\\d+) (?:element|elements)")
    public void listContainsNumberOfElements(String listName, int quantity) {
        List<SelenideElement> listOfElementsFromPage = coreScenario.getCurrentPage().getElementsList(listName);
        assertEquals(format("List size is not equal to expected: %s", listOfElementsFromPage.size()),
                     listOfElementsFromPage.size(), quantity);
    }

    @And("^the list \"([^\"]*)\" contains the element with text: \"([^\"]*)\"")
    public void listContainsElementWithText(String listName, String text) {

        assertNotNull(format("Element with text %s is not present in the list %s", text, listName),
                      findElementWithTextInList(listName, text));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * </p>
     *
     * @param blockName name of the block
     * @param listName  name of the list
     * @param varName   name of the variable
     */
    @And("^in the block \"([^\"]*)\" the list of elements \"([^\"]*)\" has been found and saved to the variable \"([^\"]*)\"$")
    public void getListElementsText(String blockName, String listName, String varName) {
        coreScenario.setVar(varName,
                            coreScenario.getCurrentPage()
                                .getBlock(blockName)
                                .getElementsList(listName)
                                .stream()
                                .map(SelenideElement::getText)
                                .collect(Collectors.toList()));
    }

    private SelenideElement findElementWithTextInList(String listName, String text) {
        List<SelenideElement> listOfElementsFromPage = coreScenario.getCurrentPage().getElementsList(listName);

        for (SelenideElement element : listOfElementsFromPage) {
            if (element.has(Condition.text(text))) {
                return element;
            }
        }
        return null;
    }
}
