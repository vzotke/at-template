package com.epam.steps.core;

import com.epam.cucumber.api.CoreScenario;
import cucumber.api.java.en.And;
import io.cucumber.datatable.DataTable;
import io.restassured.response.Response;
import lombok.extern.log4j.Log4j2;

import java.util.List;

/**
 * <h1 style="color: green; font-size: 2.2em">
 * API steps
 * </h1>
 */
@Log4j2
public class ApiSteps {

    /**
     * <p style="color: green; font-size: 1.5em">
     * Compare http response with expected
     *
     * @param variableName       Variable with saved Response
     * @param expectedStatusCode expected http status code</p>
     */
    @And("^in the response \"([^\"]*)\" statusCode: (\\d+)$")
    public void checkResponseStatusCode(String variableName, int expectedStatusCode) {
        Response response = (Response) CoreScenario.getInstance().getVar(variableName);
        response.then().statusCode(expectedStatusCode);
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Compare http response header with expected
     *
     * @param variableNameResponse Variable with saved Response
     * @param dataTable            parameters array</p>
     */
    @And("^in the response \"([^\"]*)\" is present header with values from table$")
    public void checkResponseHeaderValues(String variableNameResponse, DataTable dataTable) {
        Response response = (Response) CoreScenario.getInstance().getVar(variableNameResponse);

        for (List<String> row : dataTable.asLists()) {
            String expectedHeaderName = row.get(0);
            String expectedHeaderValue = row.get(1);

            if (expectedHeaderName.isEmpty() || expectedHeaderValue.isEmpty()) {
                throw new RuntimeException("Header and value cannot be empty");
            }
            response.then()
                .assertThat().header(expectedHeaderName, expectedHeaderValue);
        }
    }
}
