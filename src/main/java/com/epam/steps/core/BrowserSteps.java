package com.epam.steps.core;

import static com.codeborne.selenide.Selenide.clearBrowserCookies;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.switchTo;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.url;
import static com.epam.cucumber.ScopedVariables.resolveVars;
import static com.epam.steps.core.OtherSteps.getPropertyOrStringVariableOrValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.text.IsEqualIgnoringCase.equalToIgnoringCase;

import com.codeborne.selenide.WebDriverRunner;
import com.epam.cucumber.api.CoreScenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.extern.log4j.Log4j2;
import org.hamcrest.Matchers;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.Actions;

import java.util.Set;

/**
 * <h1 style="color: green; font-size: 2.2em">
 * Web steps
 * </h1>
 */
@Log4j2
public class BrowserSteps {

    private CoreScenario coreScenario = CoreScenario.getInstance();


    /**
     * <p style="color: green; font-size: 1.5em">
     * Open page via link
     *
     * @param address Find the link in property / value by key, if not found will get value from step, also all keys in
     *                braces will be replaced with values from variables from coreScenario</p>
     */
    @When("^open the page by the link: \"([^\"]*)\"$")
    public void goToUrl(String address) {
        String url = resolveVars(getPropertyOrStringVariableOrValue(address));
        open(url);
        coreScenario.write("Url = " + url);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Open page via link in the new window
     *
     * @param address Find the link in property / value by key, if not found will get value from step, also all keys in
     *                braces will be replaced with values from variables from coreScenario</p>
     */
    @When("^open the page by the link: \"([^\"]*)\" in the new window$")
    public void openUrlNewTab(String address) {
        String url = resolveVars(getPropertyOrStringVariableOrValue(address));

        ((JavascriptExecutor) WebDriverRunner.getWebDriver())
            .executeScript("window.open('" + url + "','_blank');");
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check current URL matches to the target
     *
     * @param url Find the link in property / value by key, if not found will get value from step,</p>
     */
    @Then("^current URL equals to \"([^\"]*)\"$")
    public void checkCurrentURL(String url) {
        String currentUrl = url();
        String expectedUrl = resolveVars(getPropertyOrStringVariableOrValue(url));
        assertThat("Current URL doesn't match to expected", currentUrl, is(expectedUrl));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check current URL doesn't match to the target
     *
     * @param url Find the link in property / value by key, if not found will get value from step,</p>
     */
    @Then("^current URl doesn't much \"([^\"]*)\"$")
    public void checkCurrentURLIsNotEquals(String url) {
        String currentUrl = url();
        String expectedUrl = resolveVars(getPropertyOrStringVariableOrValue(url));
        assertThat("Current URL matches to expected", currentUrl, Matchers.not(expectedUrl));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Go to the next browser tab</p>
     */
    @When("navigate to the next browser tab")
    public void switchToTheNextTab() {
        String nextWindowHandle = nextWindowHandle();
        getWebDriver().switchTo().window(nextWindowHandle);
        coreScenario.write("Current tab " + nextWindowHandle);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Refresh the page</p>
     */
    @And("^the page has been refreshed$")
    public void refreshPage() {
        refresh();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Close current tab</p>
     */
    @And("the current tab has been closed")
    public void closeCurrentTab() {
        getWebDriver().close();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Go to browser tab by the header</p>
     *
     * @param title tab header
     */
    @When("^go to the browser tab with the title \"([^\"]*)\"$")
    public void switchToTheTabWithTitle(String title) {
        switchTo().window(title);
        checkPageTitle(title);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check tab title with value specified in the step(priority: property, coreScenario, value from step)</p>
     *
     * @param pageTitleName expected title name
     */
    @Then("^the tab title is \"([^\"]*)\"$")
    public void checkPageTitle(String pageTitleName) {
        pageTitleName = getPropertyOrStringVariableOrValue(pageTitleName);
        String currentTitle = getWebDriver().getTitle().trim();
        assertThat(String.format(
            "Page title doesn't equal to expected . Expected: %s, Actual: %s",
            pageTitleName, currentTitle),
                   pageTitleName, equalToIgnoringCase(currentTitle));
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Set browser resolution</p>
     *
     * @param width  width px
     * @param height height px
     */
    @And("^set browser resolution (\\d+) х (\\d+)$")
    public void setBrowserWindowSize(int width, int height) {
        getWebDriver().manage().window().setSize(new Dimension(width, height));
        coreScenario.write("Resolution has been set: width " + width + " height" + height);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Maximise the window</p>
     */
    @And("^the browser window has been maximised$")
    public void expandWindowToFullScreen() {
        getWebDriver().manage().window().maximize();
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Scroll to the end of the page</p>
     */
    @And("^the page is scrolled down to the end$")
    public void scrollDown() {
        Actions actions = new Actions(getWebDriver());
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).build().perform();
        actions.keyUp(Keys.CONTROL).perform();
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * take screenshot and attach it to the report.</p>
     */
    @And("^the screenshot has been taken$")
    public void takeScreenshot() {
        final byte[] screenshot = ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
        CoreScenario.getInstance().getScenario().embed(screenshot, "image/png");
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * clear cookies</p>
     */
    @When("^the cookies has been deleted$")
    public void deleteCookies() {
        clearBrowserCookies();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Find cookie by name. Save cookie int variable</p>
     *
     * @param nameCookie   cookie name
     * @param variableName variable name to save the cookie
     */
    @When("^the cookie with name \"([^\"]*)\" has been saved to variable \"([^\"]*)\"$")
    public void saveCookieToVar(String nameCookie, String variableName) {
        String cookieName = resolveVars(nameCookie);
        Cookie var = getWebDriver().manage().getCookieNamed(cookieName);
        coreScenario.setVar(variableName, var);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Save all cookies to variable</p>
     *
     * @param variableName variable name to save the cookie
     */
    @When("^cookies have been saved \"([^\"]*)\"$")
    public void saveAllCookies(String variableName) {
        Set cookies = getWebDriver().manage().getCookies();
        coreScenario.setVar(variableName, cookies);
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Find cookie by name and change the valuе. Cookie name and domain will not be changed</p>
     *
     * @param cookieName  cookie name
     * @param cookieValue new value cookie
     */
    @When("^thecookie with the name \"([^\"]*)\" and value \"([^\"]*)\" has been added$")
    public void replaceCookie(String cookieName, String cookieValue) {
        String nameCookie = resolveVars(cookieName);
        String valueCookie = resolveVars(cookieValue);
        getWebDriver().manage().addCookie(new Cookie(nameCookie, valueCookie));
    }

    private String nextWindowHandle() {
        String currentWindowHandle = getWebDriver().getWindowHandle();
        Set<String> windowHandles = getWebDriver().getWindowHandles();
        windowHandles.remove(currentWindowHandle);

        return windowHandles.iterator().next();
    }
}
