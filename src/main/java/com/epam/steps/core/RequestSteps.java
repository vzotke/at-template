package com.epam.steps.core;

import com.epam.core.helpers.PropertyLoader;
import com.epam.cucumber.ScopedVariables;
import com.epam.cucumber.api.CoreScenario;
import cucumber.api.java.en.And;
import io.cucumber.datatable.DataTable;
import io.restassured.RestAssured;
import io.restassured.config.JsonConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Method;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSender;
import io.restassured.specification.RequestSpecification;

import java.util.List;

public class RequestSteps {

    private static final String
        REQUEST_URL =
        "^send (GET|PUT|POST|DELETE|HEAD|TRACE|OPTIONS|PATCH) request to URL \"([^\"]*)\"";
    public static int requestRetries = Integer.parseInt(System.getProperty("request.retries", "1"));
    private CoreScenario coreScenario = CoreScenario.getInstance();

    /**
     * <p style="color: green; font-size: 1.5em">
     * Send http request to target URL without params and BODY. Result will be saved in the target variable</p>
     *
     * @param method       Method HTTP request
     * @param address      url request (can be set directly from step or from application.properties)
     * @param variableName name of Variable to save response
     */
    @And(REQUEST_URL + ". Response has been saved to the variable: \"([^\"]*)\"$")
    public void sendHttpRequestWithoutParams(String method, String address, String variableName) {
        Response response = sendRequest(method, address, null);
        getBodyAndSaveToVariable(variableName, response);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Send http request to target URL without params and BODY. Check response code</p>
     *
     * @param method             Method HTTP request
     * @param address            url request (can be set directly from step or from application.properties)
     * @param expectedStatusCode expected response status code
     */
    @And(REQUEST_URL + ". Expected response code should be: (\\d+)$")
    public void sendHttpRequestWithoutParamsCheckResponseCode(String method, String address, int expectedStatusCode) {
        Response response = sendRequest(method, address, null);
        response.then().statusCode(expectedStatusCode);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Send http request to target URL with params/BODY. Result will be saved in the target variable</p>
     *
     * @param method       Method HTTP request
     * @param address      url request (can be set directly from step or from application.properties)
     * @param variableName name of Variable to save response
     * @param dataTable    Both in URL and Values in the table can be used variables from application.properties, and
     *                     from variable storage in CoreScenario. just add braces, e.g: http://{hostname}?user={username}.
     */
    @And(
        REQUEST_URL + " with headers and parameters from table. Response has been saved to the variable: \"([^\"]*)\"$")
    public void sendHttpRequestSaveResponse(String method, String address, String variableName, DataTable dataTable) {
        Response response = sendRequest(method, address, dataTable);
        getBodyAndSaveToVariable(variableName, response);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Send http request to target URL without params and BODY. Result will be saved in the target variable</p>
     *
     * @param method             Method HTTP request
     * @param address            url request (can be set directly from step or from application.properties)
     * @param expectedStatusCode expected response status code
     * @param dataTable          Both in URL and Values in the table can be used variables from application.properties,
     *                           and from variable storage in CoreScenario. just add braces, e.g:
     *                           http://{hostname}?user={username}.
     */
    @And(REQUEST_URL + " with headers and parameters from table. Expected response code should be: (\\d+)$")
    public void sendHttpRequestCheckResponseCode(String method, String address, int expectedStatusCode,
                                                 DataTable dataTable) {
        Response response = tryingSendRequestRetries(method, address, dataTable, expectedStatusCode);
        response.then().statusCode(expectedStatusCode);
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Send http request to target URL without params and BODY. Result will be saved in the target variable</p>
     *
     * @param method             Method HTTP request
     * @param address            url request (can be set directly from step or from application.properties)
     * @param expectedStatusCode expected response status code
     * @param variableName       name of Variable to save response
     * @param dataTable          Both in URL and Values in the table can be used variables from application.properties,
     *                           and from variable storage in CoreScenario. just add braces, e.g:
     *                           http://{hostname}?user={username}.
     */
    @And(REQUEST_URL
         + " with headers and parameters from table. Expected response code should be:(\\d+)"
         + " response has been saved to the variable: \"([^\"]*)\"$")
    public void sendHttpRequestSaveResponseCheckResponseCode(String method, String address, int expectedStatusCode,
                                                             String variableName, DataTable dataTable) {
        Response response = tryingSendRequestRetries(method, address, dataTable, expectedStatusCode);
        response.then().statusCode(expectedStatusCode);
        getBodyAndSaveToVariable(variableName, response);
    }

    private Response tryingSendRequestRetries(String method, String address, DataTable dataTable,
                                              int expectedStatusCode) {
        Response response = null;
        for (int i = 0; i < requestRetries; i++) {
            response = sendRequest(method, address, dataTable);
            if (response.statusCode() == expectedStatusCode) {
                break;
            }
        }
        return response;
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Send HTTP request</p>
     *
     * @param method    Method HTTP request
     * @param address   url request (can be set directly from step or from application.properties)
     * @param dataTable list of parameters for HTTP request
     * @return Response
     */
    private Response sendRequest(String method, String address, DataTable dataTable) {
        address = PropertyLoader.loadProperty(address, ScopedVariables.resolveVars(address));
        RestAssured.config =
            RestAssuredConfig.newConfig()
                .jsonConfig(JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));

        RequestSender request = createRequest(dataTable);
        /**/
        return request.request(Method.valueOf(method), address);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Request creation Content-Type should be set in header if needed.</p>
     *
     * @param dataTable array with parameters
     * @return created request
     */
    private RequestSender createRequest(DataTable dataTable) {
        String body = null;
        RequestSpecification request = RestAssured.given();

        if (dataTable != null) {
            for (List<String> requestParam : dataTable.asLists()) {
                String type = requestParam.get(0);
                String name = requestParam.get(1);
                String value =
                    PropertyLoader.loadValueFromFileOrPropertyOrVariableOrDefault(requestParam.get(2));
                switch (type.toUpperCase()) {
                    case "RELAXED_HTTPS": {
                        request.relaxedHTTPSValidation();
                        break;
                    }
                    case "ACCESS_TOKEN": {
                        request.header(name, "Bearer " + value.replace("\"", ""));
                        break;
                    }
                    case "PARAMETER": {
                        request.queryParam(name, value);
                        break;
                    }
                    case "MULTIPART": {
                        request.multiPart(name, value);
                        break;
                    }
                    case "FORM_PARAMETER": {
                        request.formParam(name, value);
                        break;
                    }
                    case "PATH_PARAMETER": {
                        request.pathParam(name, value);
                        break;
                    }
                    case "HEADER": {
                        request.header(name, value);
                        break;
                    }
                    case "BODY": {
                        body = ScopedVariables.resolveJsonVars(value);
                        request.body(body);
                        break;
                    }
                    default: {
                        throw new IllegalArgumentException(
                            String.format("Wrong set type %s for request parameter %s ", type, name));
                    }
                }
            }
            if (body != null) {
                coreScenario.write("Request body:\n" + body);
            }
        }

        return request;
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Get response body and save it to the variable</p>
     *
     * @param variableName name of Variable to save response
     * @param response    HTTP request response
     */
    private void getBodyAndSaveToVariable(String variableName, Response response) {
        coreScenario.setVar(variableName, response);
    }
}
