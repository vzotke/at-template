package com.epam.steps.core;

import static org.hamcrest.MatcherAssert.assertThat;

import com.epam.core.helpers.PropertyLoader;
import com.epam.cucumber.api.CoreScenario;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cucumber.api.java.en.And;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import org.hamcrest.Matchers;

import java.util.ArrayList;
import java.util.List;

public class JsonVerificationSteps {


    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that array that found by key responseName contains value</p>
     *
     * @param responseName name of Variable with saved response
     * @param key          key that array can be found
     * @param value        expected value
     */
    @And("^in the response \"([^\"]*)\" by key: \"([^\"]*)\" array contains \"([^\"]*)\"$")
    public void checkArrayHasItem(String responseName, String key, String value) {
        value = PropertyLoader.loadValueFromFileOrPropertyOrVariableOrDefault(value);
        Response response = (Response) CoreScenario.getInstance().getVar(responseName);
        List<Object> currentList = response.then().extract().path(key);
        List<String> strList = new ArrayList<>();
        currentList.forEach(e -> strList.add(e.toString()));
        assertThat("Responce doesn't contain value " + value, strList.contains(value));

    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that response that found by key responseName contains value</p>
     *
     * @param responseName name of Variable with saved response
     * @param key          key that value can be found
     * @param value        expected value
     */
    @And("^in the response \"([^\"]*)\" by key: \"([^\"]*)\" contains \"([^\"]*)\"$")
    public void checkJsonHasItem(String responseName, String key, String value) {
        value = PropertyLoader.loadValueFromFileOrPropertyOrVariableOrDefault(value);
        Response response = (Response) CoreScenario.getInstance().getVar(responseName);
        String responseKey = response.then().extract().path(key).toString();
        value = value.replace("\"", "");

        assertThat("Response doesn't contain value " + value + " expected value: " + responseKey.toString(),
                   responseKey.contains(value));

    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that array that fond by key has size = value</p>
     *
     * @param responseName name of Variable with saved response
     * @param key          key that array can be found
     * @param value        expected size
     */
    @And("^in the response \"([^\"]*)\" by key: \"([^\"]*)\" the array has size \"([^\"]*)\"$")
    public void checkArraySize(String responseName, String key, String value) {
        value = PropertyLoader.loadValueFromFileOrPropertyOrVariableOrDefault(value);
        Response response = (Response) CoreScenario.getInstance().getVar(responseName);
        assertThat(response.jsonPath().getList(key).size(), Matchers.equalTo(Integer.valueOf(value)));
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Check response body matches to json schema</p>
     *
     * @param variableName       name of Variable with saved response
     * @param expectedJsonSchema path to .json file with scheme
     */
    @And("^the response \"([^\"]*)\" match json schema: \"([^\"]*)\"$")
    public void verifyingResponseMatchesJsonScheme(String variableName, String expectedJsonSchema) {
        Response response = (Response) CoreScenario.getInstance().getVar(variableName);
        expectedJsonSchema =
            PropertyLoader.loadValueFromFileOrPropertyOrVariableOrDefault(expectedJsonSchema);

        response.then()
            .assertThat()
            .body(JsonSchemaValidator.matchesJsonSchemaInClasspath(expectedJsonSchema));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Print response to the log</p>
     **/
    @And("^print the response from variable: \"([^\"]*)\"$")
    public void printJsonFromResponse(String variableName) {
        Response response = (Response) CoreScenario.getInstance().getVar(variableName);
        CoreScenario.getInstance().write(response.getBody().prettyPrint());
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Get the value by jay and save it to scenario variables
     *
     * @param key key into JSON
     * @param jsonVar variable where was saved json
     * @param variableName name of variable to save value</p>
     */
    @And("^extract value by key \"([^\"]*)\" from json variable \"([^\"]*)\" and save to variable \"([^\"]*)\"$")
    public void extractValueFromJson(String key, String jsonVar, String variableName) {
        jsonVar = CoreScenario.getInstance().getVar(jsonVar).toString();
        JsonObject json = (JsonObject) new JsonParser().parse(jsonVar);
        CoreScenario.getInstance().setVar(variableName, json.get(key).toString());
        CoreScenario.getInstance()
            .write(variableName + " - variable with the value: "
                   + json.get(key).toString() + " has been added");
    }

}
