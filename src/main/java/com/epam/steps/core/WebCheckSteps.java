package com.epam.steps.core;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.checked;
import static com.codeborne.selenide.Condition.empty;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.selected;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.WebDriverRunner.isIE;
import static com.epam.steps.core.OtherSteps.deleteFiles;
import static com.epam.steps.core.OtherSteps.getDownloadsDir;
import static com.epam.steps.core.OtherSteps.getPropertyOrStringVariableOrValue;
import static com.epam.steps.core.WebTestConfig.DEFAULT_TIMEOUT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.ElementShould;
import com.epam.cucumber.api.CoreScenario;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.File;

/**
 * <h1 style="color: green; font-size: 2.2em">
 * WEB checks steps
 * </h1>
 *
 * <p style="color: green; font-size: 1.5em">
 * There is variable storage in coreScenario. To save/get variables setVar/getVar methods can be used. Each page should
 * be added to according class which extends CorePage. Each page/element should have english name in @Name annotation to
 * be found by human readable name not by selector. Selectors should be placed only in page class, NOT in steps. In steps
 * interaction is possible only via name from @Name annotation</p>
 */
public class WebCheckSteps {

    private CoreScenario coreScenario = CoreScenario.getInstance();

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check element appears for DEFAULT_TIMEOUT. if the property "waitingCustomElementsTimeout" in
     * application.properties is not set, default timeout is 10 seconds
     *
     * @param elementName the name of button|field|block
     *                    </p>
     */
    @Then("^the element \"([^\"]*)\" is displayed on the page$")
    public void elemIsPresentedOnPage(String elementName) {
        coreScenario.getCurrentPage().waitElementsUntil(
            Condition.appear, DEFAULT_TIMEOUT, coreScenario.getCurrentPage().getElement(elementName)
        );
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check element appears for N seconds.
     *
     * @param elementName the name of button|field|block
     * @param seconds     timeout in seconds
     *                    </p>
     */
    @Then("^the element \"([^\"]*)\" is displayed in (\\d+) (?:second|seconds)")
    public void testElementAppeared(String elementName, int seconds) {
        coreScenario.getCurrentPage().waitElementsUntil(
            Condition.appear, seconds * 1000, coreScenario.getCurrentPage().getElement(elementName)
        );
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Check element disappears for DEFAULT_TIMEOUT. if the property "waitingCustomElementsTimeout" in
     * application.properties is not set, default timeout is 10 seconds
     *
     * @param elementName the name of button|field|block
     *                    </p>
     */
    @Then("the element \"([^\"]*)\" disappears from the page")
    public void elemDisappeared(String elementName) {
        coreScenario.getCurrentPage().waitElementsUntil(
            Condition.disappears, DEFAULT_TIMEOUT, coreScenario.getCurrentPage().getElement(elementName));
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that value in element equals to value of the property
     * </p>
     *
     * @param elementName  название поля|элемента
     * @param variableName имя переменной
     */
    @Then("^the value of the (?:field|element) \"([^\"]*)\" equals to value the property \"([^\"]*)\"$")
    public void compareFieldAndVariable(String elementName, String variableName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        String expectedValue = coreScenario.getVar(variableName).toString();
        element.shouldHave(exactText(expectedValue));
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that block disappeared
     * </p>
     */
    @Then("^the (?:page|block|form) \"([^\"]*)\" disappears")
    public void blockDisappeared(String nameOfPage) {
        if (isIE()) {
            coreScenario.getPage(nameOfPage).ieDisappeared();
        } else {
            coreScenario.getPage(nameOfPage).disappeared();
        }
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that field is empty
     * </p>
     */
    @Then("^the field \"([^\"]*)\" is empty")
    public void fieldInputIsEmpty(String fieldName) {
        coreScenario.getCurrentPage().getElement(fieldName)
            .shouldHave(empty);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Save the element value to the variable
     * </p>
     */
    @When("^the value of the (?:element|field) \"([^\"]*)\" has been saved to the variable \"([^\"]*)\"$")
    public void storeElementValueInVariable(String elementName, String variableName) {
        coreScenario.setVar(variableName, coreScenario.getCurrentPage().getAnyElementText(elementName));
        coreScenario.write(
            "The Value of [" + coreScenario.getCurrentPage().getAnyElementText(elementName)
            + "] has been saved to variable ["
            + variableName + "]");
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that element is clickable
     * </p>
     */
    @Then("^the (?:field|element) \"([^\"]*)\" is clickable$")
    public void clickableField(String elementName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        element.shouldHave(enabled);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check element's attribute
     *
     * </p>
     *
     * @param elementName            name of element
     * @param attribute              expected attribute name
     * @param expectedAttributeValue expected attribute value
     */
    @Then("^the element \"([^\"]*)\" has attribute \"([^\"]*)\" with value \"(.*)\"$")
    public void checkElemContainsAtrWithValue(String elementName, String attribute, String expectedAttributeValue) {
        expectedAttributeValue = getPropertyOrStringVariableOrValue(expectedAttributeValue);
        SelenideElement currentElement = coreScenario.getCurrentPage().getElement(elementName);
        currentElement.shouldHave(attribute(attribute, expectedAttributeValue));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that the element has specific class. Can be used when element cannot be checked with standard Selenium
     * check, and you need to verify class of element. e.g. disabled
     * </p>
     */
    @Then("^the element \"([^\"]*)\" has class \"(.*)\"$")
    public void checkElemClassContainsExpectedValue(String elementName, String expectedClassValue) {
        SelenideElement currentElement = coreScenario.getCurrentPage().getElement(elementName);
        expectedClassValue = getPropertyOrStringVariableOrValue(expectedClassValue);

        currentElement.shouldHave(
            attribute("class", expectedClassValue)
        );
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that the element has NO specific class.
     * </p>
     */
    @Then("^the element \"([^\"]*)\" has no class \"(.*)\"$")
    public void checkElemClassNotContainsExpectedValue(String elementName, String expectedClassValue) {
        SelenideElement currentElement = coreScenario.getCurrentPage().getElement(elementName);

        expectedClassValue = getPropertyOrStringVariableOrValue(expectedClassValue);

        currentElement.shouldHave(
            not(attribute("class", expectedClassValue))
        );
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that the element|field contains the specific value
     * </p>
     */
    @Then("^the (?:field|element) \"([^\"]*)\" has the value \"(.*)\"$")
    public void testActualValueContainsSubstring(String elementName, String expectedValue) {
        expectedValue = getPropertyOrStringVariableOrValue(expectedValue);
        coreScenario.getCurrentPage().getElement(elementName)
            .shouldHave(text(expectedValue));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that element contains text. innerText() is used, gets visible and invisible text, trim new lines and
     * leading/ending spaces and ignore case
     * </p>
     */
    @Then("^the (?:field|element) \"([^\"]*)\" has the text \"(.*)\"$")
    public void testFieldContainsInnerText(String elementName, String expectedValue) {
        expectedValue = getPropertyOrStringVariableOrValue(expectedValue);
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        try {
            element.shouldHave(text(expectedValue));
        } catch (ElementShould ex) {
            element.shouldHave(value(expectedValue));
        }
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that element value equals to expected one
     * </p>
     */
    @Then("^the value of (?:field|element) \"([^\"]*)\" equals to \"(.*)\"$")
    public void compareValInFieldAndFromStep(String elementName, String expectedValue) {
        expectedValue = getPropertyOrStringVariableOrValue(expectedValue);
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        try {
            element.shouldHave(text(expectedValue));
        } catch (ElementShould ex) {
            element.shouldHave(value(expectedValue));
        }
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that link|button is not clickable
     * </p>
     */
    @Then("^the (?:link|button) \"([^\"]*)\" is not clickable$")
    public void buttonIsNotActive(String elementName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        assertTrue(String.format("Элемент [%s] кликабелен", elementName), element.is(Condition.disabled));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that radiobutton is selected
     * </p>
     */
    @Then("^the radiobutton \"([^\"]*)\" is selected")
    public void radioButtonIsSelected(String elementName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        element.shouldHave(selected);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that radiobutton is not selected
     * </p>
     */
    @Then("^the radiobutton \"([^\"]*)\" is not selected")
    public void radioButtonIsNotSelected(String elementName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        element.shouldHave(not(selected));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that checkbox is checked
     * </p>
     */
    @Then("^the checkbox \"([^\"]*)\" is checked")
    public void checkBoxIsChecked(String elementName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        element.shouldHave(checked);
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that checkbox is not checked
     * </p>
     */
    @Then("^the checkbox \"([^\"]*)\" is not checked")
    public void checkBoxIsNotChecked(String elementName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        element.shouldHave(not(checked));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that field is not editable
     * </p>
     */
    @Then("^(?:field|element) \"([^\"]*)\" is not editable$")
    public void fieldIsDisable(String elementName) {
        SelenideElement element = coreScenario.getCurrentPage().getElement(elementName);
        assertTrue(String.format("Element [%s] is editable", elementName),
                   element.is(Condition.disabled));
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Try to find file in /Downloads by text in the name. Can take RegEX, after verification file will be deleted
     */
    @Then("^the file \"(.*)\" has been downloaded to /Downloads folder$")
    public void testFileDownloaded(String fileName) {
        File downloads = getDownloadsDir();
        File[] expectedFiles = downloads.listFiles((files, file) -> file.contains(fileName));
        assertNotNull("File is not found", expectedFiles);
        assertNotEquals("File is not downloaded", 0, expectedFiles.length);
        assertEquals(String
                         .format(
                             "There are more than one file with name containing [%s]",
                             fileName), 1, expectedFiles.length);
        deleteFiles(expectedFiles);
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Check number of symbols in the field
     * </p>
     */
    @Then("^the field \"([^\"]*)\" contains (\\d+) symbols")
    public void checkFieldSymbolsCount(String element, int num) {
        int length = coreScenario.getCurrentPage().getAnyElementText(element).length();
        assertEquals(
            String.format("Wrong length. Expected: %s, Actual: %s", num, length),
            num, length);
    }


    /**
     * <p style="color: green; font-size: 1.5em">
     * Get element text in the block and save it to the variable
     *
     * @param elementName  name of element
     * @param blockName    name of block
     * @param variableName name of variable to save the value
     *                     </p>
     */
    @When("^the value of (?:element|field) \"([^\"]*)\" in the block \"([^\"]*)\" has been saved to the variable \"([^\"]*)\"$")
    public void saveTextElementInBlock(String elementName, String blockName, String variableName) {
        String elementText = coreScenario.getCurrentPage().getBlock(blockName).getAnyElementText(elementName);
        coreScenario.setVar(variableName, elementText);
        coreScenario.write("The value [" + elementText + "] has been saved to variable [" + variableName + "]");
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Check that the field value equals to value from variable from storage
     * </p>
     *
     * @param elementName  name of element
     * @param blockName    name of block
     * @param variableName name of variable with expected value
     */
    @Then("^the value of (?:element|field) \"([^\"]*)\" in the block \"([^\"]*)\" equals to value of \"([^\"]*)\"$")
    public void compareFieldAndVariable(String elementName, String blockName, String variableName) {
        String expectedValue = coreScenario.getVar(variableName).toString();
        coreScenario.getCurrentPage().getBlock(blockName).getElement(elementName)
            .shouldHave(exactText(expectedValue));
    }

}
