package com.epam.steps.core;

import com.epam.cucumber.api.CoreApi;
import com.epam.cucumber.api.CoreScenario;
import cucumber.api.java.en.And;
import lombok.extern.log4j.Log4j2;

/**
 * <h1 style="color: green; font-size: 2.2em">
 * Pojo seps
 * </h1>
 */
@Log4j2
public class PojoSteps {

    @And("^get JSON from POJO name \"([^\"]*)\"$")
    public void getJsonByPOJO(String name) {
        String json = CoreApi.getJsonForPojoByName(name);
        CoreScenario.getInstance().setVar(name, json);
        CoreScenario.getInstance().write(json);
    }
}
