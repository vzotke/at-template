/**
 * Copyright 2017 Alfa Laboratory Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and limitations under the
 * License.
 */
package com.epam.core.helpers;

import com.epam.cucumber.api.CoreScenario;
import com.google.common.base.Strings;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Property loader class
 */
@Log4j2
public class PropertyLoader {

    private static final String PROPERTIES_FILE = "/application.properties";
    private static final Properties PROPERTIES = getPropertiesInstance();
    private static final Properties PROFILE_PROPERTIES = getProfilePropertiesInstance();

    private PropertyLoader() {

    }

    /**
     * Return value of system property by name, in case when it's not found default value will be returned
     *
     * @param propertyName property name
     * @param defaultValue default value
     * @return value or default value
     */
    public static String loadSystemPropertyOrDefault(String propertyName, String defaultValue) {
        String propValue = System.getProperty(propertyName);
        return propValue != null ? propValue : defaultValue;
    }

    /**
     * Try to find system property by name if not found name of property will be returned as value
     *
     * @param propertyNameOrValue Name/Value of property
     * @return Property value from system properties or name as value if noy found
     */
    public static String getPropertyOrValue(String propertyNameOrValue) {
        return loadProperty(propertyNameOrValue, propertyNameOrValue);
    }

    /**
     * Return Integer value for system property, in case when it's not found default value will be returned
     *
     * @param propertyName property name
     * @param defaultValue default value
     * @return Integer value or default value
     */
    public static Integer loadSystemPropertyOrDefault(String propertyName, Integer defaultValue) {
        try {
            return Integer.valueOf(System.getProperty(propertyName, defaultValue.toString()).trim());
        } catch (NumberFormatException ex) {
            log.error("Could not parse value to Integer " + ex.getMessage());
            return defaultValue;
        }
    }

    /**
     * Return Boolean value for system property, in case when it's not found default value will be returned
     *
     * @param propertyName property name
     * @param defaultValue default value
     * @return Boolean value or default value
     */
    public static Boolean loadSystemPropertyOrDefault(String propertyName, Boolean defaultValue) {
        String def = defaultValue.toString();
        String property = loadProperty(propertyName, def);
        return Boolean.parseBoolean(property.trim());
    }

    /**
     * Return property by name from property.file
     *
     * @param propertyName name of property
     * @return property value or exception if not found
     */
    public static String loadProperty(String propertyName) {
        String value = tryLoadProperty(propertyName);
        if (null == value) {
            throw new IllegalArgumentException(
                propertyName + "has not found in the file: application.properties");
        }
        return value;
    }

    /**
     * Return value for system property, in case when it's not found default value will be returned
     *
     * @param propertyName property name
     * @param defaultValue default value
     * @return value of the property or default value
     */
    public static String loadProperty(String propertyName, String defaultValue) {
        String value = tryLoadProperty(propertyName);
        return value != null ? value : defaultValue;
    }

    /**
     * Return Integer value for system property, in case when it's not found default value will be returned
     *
     * @param propertyName property name
     * @param defaultValue default value
     * @return value of the property or default value
     */
    public static Integer loadPropertyInt(String propertyName, Integer defaultValue) {
        String value = tryLoadProperty(propertyName);
        return value != null ? Integer.parseInt(value) : defaultValue;
    }

    /**
     * Return value by property name. Find in System property at first than in property file from System property
     * "profile" if nothing found find in /application.properties
     *
     * @param propertyName property name
     * @return property value
     */
    public static String tryLoadProperty(String propertyName) {
        String value = null;
        if (!Strings.isNullOrEmpty(propertyName)) {
            String systemProperty = loadSystemPropertyOrDefault(propertyName, propertyName);
            if (!propertyName.equals(systemProperty)) {
                return systemProperty;
            }

            value = PROFILE_PROPERTIES.getProperty(propertyName);
            if (null == value) {
                value = PROPERTIES.getProperty(propertyName);
            }
        }
        return value;
    }

    /**
     * Returns properties from /application.properties
     *
     * @return Properties instance from /application.properties
     */
    @SneakyThrows(IOException.class)
    private static Properties getPropertiesInstance() {
        Properties instance = new Properties();
        try (
            InputStream resourceStream = PropertyLoader.class.getResourceAsStream(PROPERTIES_FILE);
            InputStreamReader inputStream = new InputStreamReader(resourceStream, StandardCharsets.UTF_8)
        ) {
            instance.load(inputStream);
        }
        return instance;
    }

    /**
     * Return properties from custom application.properties that set in System "profile"
     *
     * @return properties from custom application.properties, if property "profile" is not set, otherwise empty object
     */
    @SneakyThrows(IOException.class)
    private static Properties getProfilePropertiesInstance() {
        Properties instance = new Properties();
        String profile = System.getProperty("profile", "");
        if (!Strings.isNullOrEmpty(profile)) {
            String path = Paths.get(profile).toString();
            URL url = PropertyLoader.class.getClassLoader().getResource(path);
            try (
                InputStream resourceStream = url.openStream();
                InputStreamReader inputStream = new InputStreamReader(resourceStream, StandardCharsets.UTF_8)
            ) {
                instance.load(inputStream);
            }
        }
        return instance;
    }

    /**
     * Get property value form application.properties file by custom path, value from property storage of path as String
     * arg. Is used to get body.json at api steps, or get script.js at ui steps
     *
     * @param valueToFind - key to value in application.properties, path to the file as String
     * @return value as String
     */
    public static String loadValueFromFileOrPropertyOrVariableOrDefault(String valueToFind) {
        String pathAsString = StringUtils.EMPTY;
        String propertyValue = tryLoadProperty(valueToFind);
        if (StringUtils.isNotBlank(propertyValue)) {
            CoreScenario.getInstance()
                .write("Variable value " + valueToFind + " from application.properties = " + propertyValue);
            return propertyValue;
        }
        try {
            Path path = Paths.get(System.getProperty("user.dir") + valueToFind);
            pathAsString = path.toString();
            String fileValue = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
            CoreScenario.getInstance().write("Value from file " + valueToFind + " = " + fileValue);
            return fileValue;
        } catch (IOException | InvalidPathException e) {
            CoreScenario.getInstance().write("Value cannot be found by path " + pathAsString);
        }
        if (CoreScenario.getInstance().tryGetVar(valueToFind) != null) {
            return (String) CoreScenario.getInstance().getVar(valueToFind);
        }
        CoreScenario.getInstance()
            .write("Value cannot be found in storage. Default value will be used " + valueToFind);
        return valueToFind;
    }

}

