package com.epam.setup;

import static com.codeborne.selenide.Browsers.CHROME;
import static com.codeborne.selenide.Browsers.FIREFOX;
import static com.codeborne.selenide.Browsers.INTERNET_EXPLORER;
import static com.codeborne.selenide.Browsers.OPERA;
import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;
import static com.epam.setup.AtCoreSetup.app;
import static com.epam.setup.AtCoreSetup.deviceName;
import static com.epam.setup.AtCoreSetup.platformName;
import static com.epam.setup.AtCoreSetup.platformVersion;
import static org.openqa.selenium.remote.CapabilityType.PLATFORM_NAME;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.google.common.base.Strings;
import cucumber.api.Scenario;
import io.appium.java_client.AppiumDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URI;

@Log4j2
public class InitialDriver {

    /**
     * <p style="color: green; font-size: 1.5em">
     * Create WebDriver</p>
     */
    private String testDevice;

    public void startUITest(Scenario scenario) throws MalformedURLException {
        testDevice = scenarioTestDevice(scenario);

        Proxy proxy = createProxy();

        if (Strings.isNullOrEmpty(Configuration.remote)) {
            initLocalStart(proxy);
        } else {
            initRemoteStart(proxy, scenario);
        }

        if (testDevice.equals("web")) {
            WebDriverRunner.getWebDriver().manage().window().setSize(new Dimension(1920, 1080));
        }
    }

    private void initLocalStart(Proxy proxy) {
        log.info("Test have been started with OS: " + System.getProperty("os.name"));
        log.info("Test have been started with browser " + browser);
        boolean linuxOS = System.getProperty("os.name").equals("Linux");
        boolean macOS = System.getProperty("os.name").toLowerCase().contains("MAC".toLowerCase());

        if (linuxOS || macOS) {
            switch (browser) {
                case CHROME: {
                    WebDriverManager.chromedriver().setup();
                    WebDriverRunner.setWebDriver(new ChromeDriver());
                    break;
                }
                case FIREFOX: {
                    WebDriverManager.firefoxdriver().setup();
                    WebDriverRunner.setWebDriver(new FirefoxDriver());
                    break;
                }
                case OPERA: {
                    WebDriverManager.operadriver().setup();
                    WebDriverRunner.setWebDriver(new OperaDriver());
                    break;
                }
            }
        } else {
            switch (browser) {
                case INTERNET_EXPLORER: {
                    WebDriverRunner.setWebDriver(new InternetExplorerDriver());
                    break;
                }
                case CHROME: {
                    ChromeOptions options = new ChromeOptions();
                    options.setExperimentalOption("useAutomationExtension", false);
                    WebDriverRunner.setWebDriver(new ChromeDriver(options));
                    break;
                }
                case FIREFOX: {
                    WebDriverRunner.setWebDriver(new FirefoxDriver());
                    break;
                }
                case OPERA: {
                    WebDriverRunner.setWebDriver(new OperaDriver());
                    break;
                }
            }

            if (proxy != null) {
                WebDriverRunner.setProxy(proxy);
                log.info("Proxy has been added: " + proxy);
            }

        }
    }

    private void initRemoteStart(Proxy proxy, Scenario scenario) throws MalformedURLException {
        log.info("Tests have been started on remote environment " + Configuration.remote);

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", false);
        capabilities.setCapability("name", scenario.getName());

        if (proxy != null) {
            capabilities.setCapability(CapabilityType.PROXY, proxy);
            log.info("Proxy has been added: " + proxy);
        }

        if (testDevice.equals("web")) {
            capabilities.setBrowserName(Configuration.browser);
            capabilities.setBrowserName("chrome");
            capabilities.setCapability("screenResolution", "1440x900x24");
            capabilities.setCapability("width", "1440");
            capabilities.setCapability("height", "900");

            setWebDriver(
                new RemoteWebDriver(
                    URI.create(Configuration.remote).toURL(),
                    capabilities));
        }

        if (testDevice.equals("mobile")) {
            capabilities.setCapability(PLATFORM_NAME, platformName);
            capabilities.setCapability("deviceName", deviceName);
            capabilities.setCapability("platformVersion", platformVersion);
            capabilities.setCapability("app", app);

            setWebDriver(
                new AppiumDriver(
                    URI.create(Configuration.remote).toURL(),
                    capabilities));
        }

    }

    private Proxy createProxy() {
        Proxy proxy = null;
        String stringProxy = System.getProperty("proxy");
        if (!Strings.isNullOrEmpty(stringProxy)) {
            proxy = new Proxy()
                .setProxyType(Proxy.ProxyType.MANUAL)
                .setHttpProxy(stringProxy)
                .setFtpProxy(stringProxy)
                .setSslProxy(stringProxy)
            ;
        }

        return proxy;
    }

    private String scenarioTestDevice(Scenario scenario) {
        boolean web = scenario.getSourceTagNames().contains("@web");
        boolean mobile = scenario.getSourceTagNames().contains("@mobile");
        if (web && mobile) {
            Assert.fail("The Scenario has both @web and @mobile tags. Please specify the ony one of them");
        }
        if (web) {
            return "web";
        }
        if (mobile) {
            return "mobile";
        }
        return null;
    }
//

}
