package com.epam.setup;

public class AtCoreSetup {

    public final static boolean DEBUG_MODE = Boolean.getBoolean(System.getProperty("debugMode", "true"));
    public static String platformName = System.getProperty("platformName", "iOS");
    public static String deviceName = System.getProperty("deviceName", "iPhone 6s");
    public static String platformVersion = System.getProperty("platformVersion", "12.2");
    public static String app = System.getProperty("app", "");
}
