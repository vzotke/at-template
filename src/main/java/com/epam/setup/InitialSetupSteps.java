/**
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * <p style="color: green; font-size: 1.5em">
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p style="color: green; font-size: 1.5em">
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.epam.setup;

import static com.epam.core.helpers.PropertyLoader.loadProperty;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.epam.cucumber.api.CoreEnvironment;
import com.epam.cucumber.api.CoreScenario;
import com.epam.cucumber.utils.AllureEnvironmentBuilder;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.qameta.allure.Attachment;
import io.qameta.allure.restassured.AllureRestAssured;
import io.qameta.allure.selenide.AllureSelenide;
import io.restassured.RestAssured;
import lombok.experimental.Delegate;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.logging.LogType;

import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Level;

/**
 * <h1 style="color: green; font-size: 2.2em">
 * Set Up
 * </h1>
 */
@Log4j2
public class InitialSetupSteps {

    /**
     * <p>Turn on Allure listeners</p>
     */
    static {
        log.info("Selenide Allure logger has been added");
        SelenideLogger.addListener("allure", new AllureSelenide().savePageSource(false));
        log.info("RestAssured Allure filter has been added");
        RestAssured.filters(new AllureRestAssured());
    }

    @Delegate
    CoreScenario coreScenario = CoreScenario.getInstance();

    @Attachment(value = "Web Driver Logs",
        type = "text/plain",
        fileExtension = ".log")
    private static String attachmentWebDriverLogs() {
        /**
         * To wait all console logs are loaded
         */
        Selenide.sleep(1000);
        List<String> webDriverLogs = Selenide.getWebDriverLogs(LogType.BROWSER, Level.ALL);
        StringBuilder stringBuilder = new StringBuilder();
        for (String log : webDriverLogs) {
            stringBuilder.append(log);
            stringBuilder.append("\n\n");
            CoreScenario.getInstance().write(log);
        }

        return stringBuilder.toString();
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * Before each test</p>
     * <p>Trun on Allure listeners</p>
     * <p>If scenario has tag "@web" or "@mobile" WebDriver will be created</p>
     * <p>Creates environment to scenario execution</p>
     */
    @Before
    public void beforeEachTest(Scenario scenario) throws MalformedURLException {
        RestAssured.baseURI = System.getProperty("baseURI", loadProperty("baseURI"));
        Configuration.baseUrl = System.getProperty("uiBaseURI", loadProperty("uiBaseURI"));
        Configuration.remote = System.getProperty("remoteURL", loadProperty("remoteURL"));

        /**
         * If scenario has tag "@web" or "@mobile" WebDriver will be created
         */
        boolean uiTest =
            scenario.getSourceTagNames().contains("@web") ||
            scenario.getSourceTagNames().contains("@mobile");
        if (uiTest) {
            new InitialDriver().startUITest(scenario);
        }

        /**
         * СCreates environment to scenario execution
         *
         * @param scenario scenario
         * @throws Exception
         */
        coreScenario.setEnvironment(new CoreEnvironment(scenario, uiTest));
    }

    /**
     * <p style="color: green; font-size: 1.5em">
     * If scenario has tag "@web" or "@mobile" deletes cookies and closes the browser </p>
     */
    @After
    public void afterEachTest(Scenario scenario) {

        if (scenario.getSourceTagNames().contains("@web")) {
            AllureEnvironmentBuilder.getAllureEnvironment();
            attachmentWebDriverLogs();
            Selenide.clearBrowserLocalStorage();
            Selenide.clearBrowserCookies();
            WebDriverRunner.getWebDriver().close();
        }
        if (scenario.getSourceTagNames().contains("@mobile")) {
            AllureEnvironmentBuilder.getAllureEnvironment();
            WebDriverRunner.getWebDriver().quit();
        }
        coreScenario.removeEnvironment();
    }
}
