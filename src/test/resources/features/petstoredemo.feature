@all
@api
Feature:Demo how to create read update and delete pet with fake API using test framework

  Scenario: CRUD pet
    Given get JSON from POJO name "new_pet"
    And extract value by key "id" from json variable "new_pet" and save to variable "pet_id"
    And extract value by key "name" from json variable "new_pet" and save to variable "pet_name"
    When send POST request to URL "/pet" with headers and parameters from table. Response has been saved to the variable: "pet_resp"
      | HEADER | api_key      | tempauto         |
      | HEADER | accept       | application/json |
      | HEADER | Content-Type | application/json |
      | BODY   | new_pet      | new_pet          |
    And in the response "pet_resp" statusCode: 200

#   Read: verify that pet has been created in available list
    Given send GET request to URL "/pet/findByStatus" with headers and parameters from table. Response has been saved to the variable: "pet_resp"
      | HEADER    | api_key | tempauto  |
      | PARAMETER | status  | available |
    Then in the response "pet_resp" by key: "id" array contains "pet_id"

#   Read: verify that newly created pet can be found by ID
    Given send GET request to URL "/pet/{pet_id}". Response has been saved to the variable: "pet_resp"
    And print the response from variable: "pet_resp"
    Then in the response "pet_resp" by key: "id" contains "pet_id"
    Then in the response "pet_resp" by key: "name" contains "pet_name"

#   Update
    Given get JSON from POJO name "Pet_to_update"
    And extract value by key "id" from json variable "Pet_to_update" and save to variable "updated_pet_id"
    And extract value by key "name" from json variable "Pet_to_update" and save to variable "updated_pet_name"
    When send PUT request to URL "/pet" with headers and parameters from table. Response has been saved to the variable: "pet_resp"
      | HEADER | api_key       | tempauto         |
      | HEADER | accept        | application/json |
      | HEADER | Content-Type  | application/json |
      | BODY   | Pet_to_update | Pet_to_update    |
    And print the response from variable: "pet_resp"
    Then in the response "pet_resp" by key: "id" contains "updated_pet_id"
    Then in the response "pet_resp" by key: "name" contains "updated_pet_name"

#   Delete
    Given send DELETE request to URL "/pet/{updated_pet_id}". Expected response code should be: 200
    Then send GET request to URL "/pet/{updated_pet_id}". Expected response code should be: 404



