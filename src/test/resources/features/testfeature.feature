@all
@web
Feature:Demo UI test

  Scenario:Demo test to show how to use FW for UI testing
    Given open the page "Main Page" by the link "{uiBaseURI}/"
    When in the field "Search" send value "Epam systems"
    And send keyboard button "Enter"
    Then the page "Search results" is loaded
    And the list "List of results" contains the element with text: "Epam"
    And click on link "The first search result"
    And the page "Epam main page" is loaded
