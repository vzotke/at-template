package com.epam;

import com.epam.cucumber.utils.AllureEnvironmentBuilder;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import lombok.extern.log4j.Log4j2;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@Log4j2
@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"pretty"},
    features = "src/test/resources/features",
    glue = {"com/epam"},
    tags = {"@all"})
public class RunCukesTest {
}





