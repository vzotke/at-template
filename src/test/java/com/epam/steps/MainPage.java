package com.epam.steps;

import com.codeborne.selenide.SelenideElement;
import com.epam.cucumber.annotations.Name;
import com.epam.cucumber.api.CorePage;
import org.openqa.selenium.support.FindBy;

@Name("Main Page")
public class MainPage extends CorePage {

    @Name("Search")
    @FindBy(css = "input[name=q]")
    private SelenideElement searchField;


}
