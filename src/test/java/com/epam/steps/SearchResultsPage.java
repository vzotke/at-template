package com.epam.steps;

import com.codeborne.selenide.SelenideElement;
import com.epam.cucumber.annotations.Name;
import com.epam.cucumber.api.CorePage;
import org.openqa.selenium.support.FindBy;

import java.util.List;

@Name("Search results")
public class SearchResultsPage extends CorePage {

    @Name("List of results")
    @FindBy(css = ".rc")
    private List<SelenideElement> listOfResults;

    @Name("The first search result")
    @FindBy(css = ".rc .r a[href]")
    private SelenideElement firstLinkInSearchResult;

}
