package com.epam.steps;

import com.codeborne.selenide.SelenideElement;
import com.epam.cucumber.annotations.Name;
import com.epam.cucumber.api.CorePage;
import org.openqa.selenium.support.FindBy;

@Name("Epam main page")
public class EpamMainPage extends CorePage {

    @Name("Footer")
    @FindBy(css = ".footer-ui")
    private SelenideElement footer;

}
