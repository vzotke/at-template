Automation template
=========================

Automation template is a template based on [alfa-laboratory/akita](https://github.com/alfa-laboratory/akita) and
 [Antonppavlov/at-library](https://github.com/Antonppavlov/at-library) projects
 
It's a BDD style template based on Cucumber, Selenide, Junit4. Hamcrest, Rest-Assured, Appium, Webdrivermanager and Allure.
Tests can be created as feature files in BDD style, that also can be used as a user's documentation.

Example:
```
Feature:Demo UI test
  Scenario:Demo test to show how to use FW for UI testing
    Given open the page "Main Page" by the link "{uiBaseURI}/"
    When in the field "Search" send value "Epam systems"
    And send keyboard button "Enter"
    Then the page "Search results" is loaded
```

The main changes and differences from Akita and at-library:
=========================
- Gradle has been changed to Maven (Thanks [Anton Pavlov](https://github.com/Antonppavlov) for that)
- Full translation to English (steps, comments and log records)
- PojoName annotation has been added to have possibility to work with POJO via human readable names using reflection
    (PojoController needs to be refactored)


application.properties
=========================
This properties file has been used to set additional properties such as baseURI, remoteURI, user credentials etc.
In scenario when we use any property, property loader tries to find it into property file, then in scenario properties.

UI tests
=========================
There is variable storage in coreScenario. To save/get variables setVar/getVar methods can be used. Each page should
be added to according class which extends CorePage. Each page/element should have english name in @Name annotation to
be found by human readable name not by selector. Selectors should be placed only in page class, NOT in steps. In steps
interaction is possible only via name from @Name annotation. Also each element visibility will be checked during page
initialization, to exclude element from verification you can use @optional annotation. Default steps can be found at
  `com.epam.steps.core`
 
Example
 ```
@Name("Search results")
public class SearchResultsPage extends CorePage {

    @Name("List of results")
    @FindBy(css = ".rc")
    private List<SelenideElement> listOfResults;

    @Name("The first search result")
    @FindBy(css = ".rc .r a[href]")
    private SelenideElement firstLinkInSearchResult;

}
```

Page elements interaction
=========================

To have access to page elements the page need to be set as current.
The page can be set as current via step:

```@Then("^the (?:page|block|form|tab) \"([^\"]*)\" is loaded$")```

API tests
=========================
There is possibility to send `GET|PUT|POST|DELETE|HEAD|TRACE|OPTIONS|PATCH` requests and save response to scenario variable
Also is possible to create POJO with @PojoName annotation and use it in steps
```
    When send POST request to URL "/pet" with headers and parameters from table. Response has been saved to the variable: "pet_resp"
      | HEADER | api_key      | tempauto         |
      | HEADER | accept       | application/json |
      | HEADER | Content-Type | application/json |
      | BODY   | new_pet      | new_pet       
```

Data tables can contain the next params: 

`RELAXED_HTTPS, ACCESS_TOKEN, PARAMETER, MULTIPART, FORM_PARAMETER, PATH_PARAMETER, PATH_PARAMETER, HEADER, BODY`

Logging
=========================
To add additional log info to report the next method can be used

```
coreScenario.write(Object object);
```

Remote run
=========================
To run test on remote Selenoid or Selenium hub just add -DremoteURL={target_URL} param, or change remoteURL in 
application.properties

